﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;

namespace Editor3D
{
    public abstract class Renderer
    {
        public Camera camera;
        public Bitmap bitmap;
        public Color ambientColor;
        
        public Renderer(Camera camera, Bitmap bitmap, Color ambientColor)
        {
            this.camera = camera;
            this.bitmap = bitmap;
            this.ambientColor = ambientColor;
        }

        public abstract void Render();
        
        protected virtual void Clear(BitmapData bitmapData)
        {
            for (int y = 0; y < bitmapData.Height; y++)
                for (int x = 0; x < bitmapData.Width; x++)
                    SetPixel(bitmapData, x, y, ambientColor);
        }
        
        protected class Point2D
        {
            public int x;
            public int y;
            public double z;
            public Color color;
            public float UV_X = -1;
            public float UV_Y = -1;

            public Point2D(int x, int y, double z, Color color)
            {
                this.x = x;
                this.y = y;
                this.z = z;
                this.color = color;
            }

            public Point2D(int x, int y, double z, Color color, float UV_X, float UV_Y)
            {
                this.x = x;
                this.y = y;
                this.z = z;
                this.color = color;
                this.UV_X = UV_X;
                this.UV_Y = UV_Y;
            }

            public static explicit operator Point2D(Point3D point)
                => new Point2D((int) point.X, (int) point.Y, point.Z, point.Color);

            public override string ToString()
            {
                return $"X: {x}, Y: {y}, Z: {z}, UVX: {UV_X}, UVY: {UV_Y}";
            }
        }
        
        protected virtual unsafe void SetPixel(BitmapData bitmapData, int x, int y, Color color)
        {
            if (x < 0 || x >= bitmapData.Width || y < 0 || y >= bitmapData.Height)
                return;
            byte* bitmapPointer = (byte*)bitmapData.Scan0.ToPointer();
            bitmapPointer[bitmapData.Stride * y + 3 * x] = color.B;
            bitmapPointer[bitmapData.Stride * y + 3 * x + 1] = color.G;
            bitmapPointer[bitmapData.Stride * y + 3 * x + 2] = color.R;
        }
    }
}