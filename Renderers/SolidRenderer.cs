﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;

namespace Editor3D
{
    public class SolidRenderer : WireframeRenderer
    {
        public Color boundaryColor;

        public SolidRenderer(IEnumerable<IPrimitive> primitives, Camera camera, Bitmap bitmap, Color ambientColor,
            bool visibleOnly, Color boundaryColor)
            : base(primitives, camera, bitmap, ambientColor, visibleOnly)
        {
            this.boundaryColor = boundaryColor;
        }

        protected List<Point2D> RasterizeFacetBase(Facet facet)
        {
            return base.RasterizeFacet(facet);
        }

        protected override List<Point2D> RasterizeFacet(Facet facet)
        {
            List<List<Point2D>> rows = RasterizeFacetBase(facet)
                .GroupBy(point => point.y)
                .Select(group => group
                    .OrderBy(point => point.x)
                    .ToList())
                .ToList();
            
            List<Point2D> points = new List<Point2D>();
            List<Point2D> boundaryPoints = new List<Point2D>();
            foreach (List<Point2D> rowPoints in rows)
            {
                int y = rowPoints[0].y;
                points.Add(rowPoints[0]);
                boundaryPoints.Add(points.Last());
                for (int i = 1; i < rowPoints.Count; i++)
                {
                    if (rowPoints[i].x - rowPoints[i - 1].x > 1)
                        for (int x = rowPoints[i - 1].x + 1; x <= rowPoints[i].x - 1; x++)
                        {
                            double t = (double) (x - rowPoints[i - 1].x) / (rowPoints[i].x - rowPoints[i - 1].x);
                            double z = t * (rowPoints[i].z - rowPoints[i - 1].z) + rowPoints[i - 1].z;
                            int red = (int) (t * (rowPoints[i].color.R - rowPoints[i - 1].color.R) +
                                             rowPoints[i - 1].color.R);
                            int green = (int) (t * (rowPoints[i].color.G - rowPoints[i - 1].color.G) +
                                               rowPoints[i - 1].color.G);
                            int blue = (int) (t * (rowPoints[i].color.B - rowPoints[i - 1].color.B) +
                                              rowPoints[i - 1].color.B);
                            Color color = Color.FromArgb(red, green, blue);
                            points.Add(new Point2D(x, y, z, color));
                        }
                    points.Add(rowPoints[i]);
                    boundaryPoints.Add(points.Last());
                }
                if (!boundaryColor.IsEmpty)
                    foreach (Point2D boundaryPoint in boundaryPoints)
                        boundaryPoint.color = boundaryColor;
            }
            return points;
        }

        protected override void Draw(List<List<Point2D>> rasterizedPrimitives, BitmapData bitmapData)
        {
            Clear(bitmapData);
            double[,] zBuffer = new double[bitmapData.Width, bitmapData.Height];
            for (int y = 0; y < bitmapData.Height; y++)
                for (int x = 0; x < bitmapData.Width; x++)
                    zBuffer[x, y] = double.MaxValue;
            foreach (List<Point2D> pointList in rasterizedPrimitives)
                foreach (Point2D point in pointList)
                    if (0 <= point.x && point.x < bitmapData.Width && 0 <= point.y && point.y < bitmapData.Height)
                        if (point.z < zBuffer[point.x, point.y])
                        {
                            SetPixel(bitmapData, point.x, point.y, point.color);
                            zBuffer[point.x, point.y] = point.z;
                        }
        }
    }
}