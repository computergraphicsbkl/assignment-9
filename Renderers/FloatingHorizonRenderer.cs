﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using org.mariuszgromada.math.mxparser;
using System;

namespace Editor3D
{
    public class FloatingHorizonRenderer : Renderer
    {
        public string function;
        public double minX, maxX;
        public double minZ, maxZ;
        public int xTicks, zTicks;
        public Color upperColor, lowerColor;
		public double rotationY;

        public FloatingHorizonRenderer(string function, Camera camera, Bitmap bitmap, Color ambientColor, double minX,
            double maxX, double minZ, double maxZ, int xTicks, int zTicks, Color upperColor, Color lowerColor, double rotationY)
            : base(camera, bitmap, ambientColor)
        {
            this.function = function;
            this.minX = minX;
            this.maxX = maxX;
            this.minZ = minZ;
            this.maxZ = maxZ;
            this.xTicks = xTicks;
            this.zTicks = zTicks;
            this.upperColor = upperColor;
            this.lowerColor = lowerColor;
			this.rotationY = rotationY;
        }

        public override void Render()
        {
            BitmapData bitmapData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            Clear(bitmapData);

            Argument xArgument = new Argument("x");
            Argument zArgument = new Argument("z");
            Expression expression = new Expression(function, xArgument, zArgument);

            double deltaX = (maxX - minX) / xTicks;
            double deltaZ = (maxZ - minZ) / zTicks;
            
            List<int> minHorizon = new List<int>(bitmap.Width);
            List<int> maxHorizon = new List<int>(bitmap.Width);
            for (int x = 0; x < bitmap.Width; x++)
            {
                minHorizon.Add(bitmap.Height);
                maxHorizon.Add(0);
            }
            
            double z = maxZ;
            for (int i = 0; i <= zTicks; i++)
            {
                double x = minX;
                double oldX = 0;
                double oldY = 0;
                double oldZ = 0;
                for (int j = 0; j <= xTicks; j++)
                {
					Point3D point = new Point3D(x, 0, z).Transform(Transformation.RotateY(rotationY * Math.PI / 180));
                    xArgument.setArgumentValue(point.X);
                    zArgument.setArgumentValue(point.Z);
                    double y = expression.calculate();

					Point3D transformedPoint = new Point3D(point.X, y, point.Z)
                        .Transform(camera.Projection)
                        .NormalizedToDisplay(bitmap.Width, bitmap.Height);
                    
                    if (j != 0)
                        DrawLine(bitmapData, new Line(new Point3D(oldX, oldY, oldZ), transformedPoint), minHorizon,
                            maxHorizon);

                    oldX = transformedPoint.X;
                    oldY = transformedPoint.Y;
                    oldZ = transformedPoint.Z;
                    x += deltaX;
                }
                z -= deltaZ;
            }

            bitmap.UnlockBits(bitmapData);
        }

        private void DrawLine(BitmapData bitmapData, Line line, List<int> minHorizon, List<int> maxHorizon)
        {
            Point2D point1, point2;
            if ((int) line.A.X < (int) line.B.X)
            {
                point1 = (Point2D) line.A;
                point2 = (Point2D) line.B;
            }
            else
            {
                point1 = (Point2D) line.B;
                point2 = (Point2D) line.A;
            }
            int dx = point2.x - point1.x;
            int dy = point2.y - point1.y;
            double gradient = 0;
            if (dx == 0 && dy > 0)
                gradient = 1;
            else if (dx == 0 && dy < 0)
                gradient = -1;
            else if (dx == 0 && dy == 0)
            {
                if (0 <= point1.x && point1.x < bitmapData.Width && 0 <= point1.y && point1.y < bitmapData.Height)
                {
                    SetPixel(bitmapData, point1.x, point1.y, lowerColor);
                    return;
                }
            }
            else
                gradient = (double)dy / dx;
            
            if (gradient >= 1)
            {
                int x = point1.x;
                int y = point1.y;
                double d = 2 * dx - dy;
                while (y <= point2.y)
                {
                    double t = (double) (y - point1.y) / (point2.y - point1.y);
                    double z = t * (point2.z - point1.z) + point1.z;
                    if (z >= 0)
                        if (0 <= x && x < bitmapData.Width && 0 <= y && y < bitmapData.Height)
                        {
                            if (y > maxHorizon[x])
                            {
                                maxHorizon[x] = y;
                                SetPixel(bitmapData, x, y, upperColor);
                            }
                            if (y < minHorizon[x])
                            {
                                minHorizon[x] = y;
                                SetPixel(bitmapData, x, y, lowerColor);
                            }
                        }
                    if (d < 0)
                        d += 2 * dx;
                    else
                    {
                        x++;
                        d += 2 * (dx - dy);
                    }
                    y++;
                }
            }
            else if (0 <= gradient && gradient < 1)
            {
                int x = point1.x;
                int y = point1.y;
                double d = 2 * dy - dx;
                while (x <= point2.x)
                {
                    double t = (double)(x - point1.x) / (point2.x - point1.x);
                    double z = t * (point2.z - point1.z) + point1.z;
                    if (z >= 0)
                        if (0 <= x && x < bitmapData.Width && 0 <= y && y < bitmapData.Height)
                        {
                            if (y > maxHorizon[x])
                            {
                                maxHorizon[x] = y;
                                SetPixel(bitmapData, x, y, upperColor);
                            }
                            if (y < minHorizon[x])
                            {
                                minHorizon[x] = y;
                                SetPixel(bitmapData, x, y, lowerColor);
                            }
                        }
                    if (d < 0)
                        d += 2 * dy;
                    else
                    {
                        y++;
                        d += 2 * (dy - dx);
                    }
                    x++;
                }
            }
            else if (-1 < gradient && gradient < 0)
            {
                dy = -dy;
                int x = point1.x;
                int y = point1.y;
                double d = 2 * dy - dx;
                while (x <= point2.x)
                {
                    double t = (double)(x - point1.x) / (point2.x - point1.x);
                    double z = t * (point2.z - point1.z) + point1.z;
                    if (z >= 0)
                        if (0 <= x && x < bitmapData.Width && 0 <= y && y < bitmapData.Height)
                        {
                            if (y > maxHorizon[x])
                            {
                                maxHorizon[x] = y;
                                SetPixel(bitmapData, x, y, upperColor);
                            }
                            if (y < minHorizon[x])
                            {
                                minHorizon[x] = y;
                                SetPixel(bitmapData, x, y, lowerColor);
                            }
                        }
                    if (d < 0)
                        d += 2 * dy;
                    else
                    {
                        y--;
                        d += 2 * (dy - dx);
                    }
                    x++;
                }
            }
            else
            {
                dy = -dy;
                int x = point1.x;
                int y = point1.y;
                double d = 2 * dx - dy;
                while (y >= point2.y)
                {
                    double t = (double) (y - point1.y) / (point2.y - point1.y);
                    double z = t * (point2.z - point1.z) + point1.z;
                    if (z >= 0)
                        if (0 <= x && x < bitmapData.Width && 0 <= y && y < bitmapData.Height)
                        {
                            if (y > maxHorizon[x])
                            {
                                maxHorizon[x] = y;
                                SetPixel(bitmapData, x, y, upperColor);
                            }
                            if (y < minHorizon[x])
                            {
                                minHorizon[x] = y;
                                SetPixel(bitmapData, x, y, lowerColor);
                            }
                        }
                    if (d < 0)
                        d += 2 * dx;
                    else
                    {
                        x++;
                        d += 2 * (dx - dy);
                    }
                    y--;
                }
            }
        }
    }
}