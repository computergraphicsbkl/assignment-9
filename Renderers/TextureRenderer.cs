﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using Editor3D.Properties;

namespace Editor3D.Renderers
{
    class TextureRenderer : SolidRenderer
    {
        const float topTrianglePoint_X = 0.5f;
        const float topTrianglePoint_Y = 0.86602540378444f;
        Bitmap texture = Resources.texture;

        public TextureRenderer(IEnumerable<IPrimitive> primitives, Camera camera, Bitmap bitmap, Color ambientColor,
            bool visibleOnly, Color boundaryColor)
            : base(primitives, camera, bitmap, ambientColor, visibleOnly, boundaryColor) { }

        protected override List<IPrimitive> Transform(IEnumerable<IPrimitive> primitives, Camera camera)
        {
            List<IPrimitive> transformedPrimitives = new List<IPrimitive>();
            foreach (IPrimitive primitive in primitives)
                if (primitive is Point3D point)
                    transformedPrimitives.Add(point
                        .Transform(camera.Projection)
                        .NormalizedToDisplay(bitmap.Width, bitmap.Height));
                else if (primitive is Light light)
                    transformedPrimitives.Add(light
                        .Transform(camera.Projection)
                        .NormalizedToDisplay(bitmap.Width, bitmap.Height));
                else if (primitive is Line line)
                    transformedPrimitives.Add(new Line(
                        line.A
                            .Transform(camera.Projection)
                            .NormalizedToDisplay(bitmap.Width, bitmap.Height),
                        line.B
                            .Transform(camera.Projection)
                            .NormalizedToDisplay(bitmap.Width, bitmap.Height)));
                else if (primitive is Facet facet)
                    transformedPrimitives.Add(new Facet(
                        facet.points.Select(
                            facetPoint => facetPoint
                                .Transform(camera.Projection)
                                .NormalizedToDisplay(bitmap.Width, bitmap.Height)).ToList()));
                else if (primitive is Polyhedron polyhedron)
                {
                    Polyhedron transformedPolyhedron = polyhedron.Copy();
                    foreach (Point3D polyhedronPoint in transformedPolyhedron.points)
                    {
                        polyhedronPoint.Apply(camera.Projection);
                        polyhedronPoint.NormalizeToDisplay(bitmap.Width, bitmap.Height);
                    }
                    foreach (Facet facet_ in transformedPolyhedron.facets)
                    {
                        if (facet_.points.Count == 3)
                        {
                            facet_.UV_coords.Add(new PointF(0, 0));
                            facet_.UV_coords.Add(new PointF(1, 0));
                            facet_.UV_coords.Add(new PointF(topTrianglePoint_X, topTrianglePoint_Y));
                        }
                        else if (facet_.points.Count == 4)
                        {
                            facet_.UV_coords.Add(new PointF(0, 0));
                            facet_.UV_coords.Add(new PointF(1, 0));
                            facet_.UV_coords.Add(new PointF(1, 1));
                            facet_.UV_coords.Add(new PointF(0, 1));
                        }
                        else if (facet_.points.Count == 5)
                        {
                            facet_.UV_coords.Add(new PointF(0.191358f, 0));
                            facet_.UV_coords.Add(new PointF(0.8086419f, 0));
                            facet_.UV_coords.Add(new PointF(1, 0.5864198f));
                            facet_.UV_coords.Add(new PointF(0.5f, 0.9506173f));
                            facet_.UV_coords.Add(new PointF(0, 0.5864198f));
                        }
                    }
                    transformedPrimitives.Add(transformedPolyhedron);
                }
            return transformedPrimitives;
        }




        protected List<Point2D> RasterizeLine(Line line, PointF UV_coordsA, PointF UV_coordsB)
        {
            List<Point2D> points = new List<Point2D>();
            Point2D point1, point2;
            if ((int)line.A.X < (int)line.B.X)
            {
                point1 = (Point2D)line.A;
                point1.UV_X = UV_coordsA.X;
                point1.UV_Y = UV_coordsA.Y;
                point2 = (Point2D)line.B;
                point2.UV_X = UV_coordsB.X;
                point2.UV_Y = UV_coordsB.Y;
            }
            else
            {
                point1 = (Point2D)line.B;
                point1.UV_X = UV_coordsB.X;
                point1.UV_Y = UV_coordsB.Y;
                point2 = (Point2D)line.A;
                point2.UV_X = UV_coordsA.X;
                point2.UV_Y = UV_coordsA.Y;
            }
            int dx = point2.x - point1.x;
            int dy = point2.y - point1.y;
            double gradient;
            if (dx == 0 && dy > 0)
                gradient = 1;
            else if (dx == 0 && dy < 0)
                gradient = -1;
            else if (dx == 0 && dy == 0)
                return new List<Point2D>(new[] { point1 });
            else
                gradient = (double)dy / dx;

            if (gradient >= 1)
            {
                int x = point1.x;
                int y = point1.y;
                double d = 2 * dx - dy;
                while (y <= point2.y)
                {
                    double t = (double)(y - point1.y) / (point2.y - point1.y);
                    double z = t * (point2.z - point1.z) + point1.z;
                    int red = (int)(t * (point2.color.R - point1.color.R) + point1.color.R);
                    int green = (int)(t * (point2.color.G - point1.color.G) + point1.color.G);
                    int blue = (int)(t * (point2.color.B - point1.color.B) + point1.color.B);
                    Color color = Color.FromArgb(red, green, blue);
                    float UV_X = Geometry.LinearInterpolation(point1.UV_X, point2.UV_X, (float)t);
                    float UV_Y = Geometry.LinearInterpolation(point1.UV_Y, point2.UV_Y, (float)t);
                    if (z >= 0)
                        points.Add(new Point2D(x, y, z, color, UV_X, UV_Y));
                    if (d < 0)
                        d += 2 * dx;
                    else
                    {
                        x++;
                        d += 2 * (dx - dy);
                    }
                    y++;
                }
            }
            else if (0 <= gradient && gradient < 1)
            {
                int x = point1.x;
                int y = point1.y;
                double d = 2 * dy - dx;
                while (x <= point2.x)
                {
                    double t = (double)(x - point1.x) / (point2.x - point1.x);
                    double z = t * (point2.z - point1.z) + point1.z;
                    int red = (int)(t * (point2.color.R - point1.color.R) + point1.color.R);
                    int green = (int)(t * (point2.color.G - point1.color.G) + point1.color.G);
                    int blue = (int)(t * (point2.color.B - point1.color.B) + point1.color.B);
                    Color color = Color.FromArgb(red, green, blue);
                    float UV_X = Geometry.LinearInterpolation(point1.UV_X, point2.UV_X, (float)t);
                    float UV_Y = Geometry.LinearInterpolation(point1.UV_Y, point2.UV_Y, (float)t);
                    if (z >= 0)
                        points.Add(new Point2D(x, y, z, color, UV_X, UV_Y));
                    if (d < 0)
                        d += 2 * dy;
                    else
                    {
                        y++;
                        d += 2 * (dy - dx);
                    }
                    x++;
                }
            }
            else if (-1 < gradient && gradient < 0)
            {
                dy = -dy;
                int x = point1.x;
                int y = point1.y;
                double d = 2 * dy - dx;
                while (x <= point2.x)
                {
                    double t = (double)(x - point1.x) / (point2.x - point1.x);
                    double z = t * (point2.z - point1.z) + point1.z;
                    int red = (int)(t * (point2.color.R - point1.color.R) + point1.color.R);
                    int green = (int)(t * (point2.color.G - point1.color.G) + point1.color.G);
                    int blue = (int)(t * (point2.color.B - point1.color.B) + point1.color.B);
                    Color color = Color.FromArgb(red, green, blue);
                    float UV_X = Geometry.LinearInterpolation(point1.UV_X, point2.UV_X, (float)t);
                    float UV_Y = Geometry.LinearInterpolation(point1.UV_Y, point2.UV_Y, (float)t);
                    if (z >= 0)
                        points.Add(new Point2D(x, y, z, color, UV_X, UV_Y));
                    if (d < 0)
                        d += 2 * dy;
                    else
                    {
                        y--;
                        d += 2 * (dy - dx);
                    }
                    x++;
                }
            }
            else
            {
                dy = -dy;
                int x = point1.x;
                int y = point1.y;
                double d = 2 * dx - dy;
                while (y >= point2.y)
                {
                    double t = (double)(y - point1.y) / (point2.y - point1.y);
                    double z = t * (point2.z - point1.z) + point1.z;
                    int red = (int)(t * (point2.color.R - point1.color.R) + point1.color.R);
                    int green = (int)(t * (point2.color.G - point1.color.G) + point1.color.G);
                    int blue = (int)(t * (point2.color.B - point1.color.B) + point1.color.B);
                    Color color = Color.FromArgb(red, green, blue);
                    float UV_X = Geometry.LinearInterpolation(point1.UV_X, point2.UV_X, (float)t);
                    float UV_Y = Geometry.LinearInterpolation(point1.UV_Y, point2.UV_Y, (float)t);
                    if (z >= 0)
                        points.Add(new Point2D(x, y, z, color, UV_X, UV_Y));
                    if (d < 0)
                        d += 2 * dx;
                    else
                    {
                        x++;
                        d += 2 * (dx - dy);
                    }
                    y--;
                }
            }

            return points;
        }



        protected List<Point2D> RasterizeFacetWireframe(Facet facet)
        {
            List<Point2D> boundary = new List<Point2D>();
            for (int i = 0; i < facet.points.Count; i++)
                boundary.AddRange(RasterizeLine(new Line(
                    facet.points[i],
                    facet.points[(i + 1) % facet.points.Count]),
                    facet.UV_coords[i], facet.UV_coords[(i + 1) % facet.points.Count]));
            return boundary;
        }



        protected override List<Point2D> RasterizeFacet(Facet facet)
        {
            List<List<Point2D>> rows = RasterizeFacetWireframe(facet)
                .GroupBy(point => point.y)
                .Select(group => group
                    .OrderBy(point => point.x)
                    .ToList())
                .ToList();

            List<Point2D> points = new List<Point2D>();
            List<Point2D> boundaryPoints = new List<Point2D>();
            foreach (List<Point2D> rowPoints in rows)
            {
                int y = rowPoints[0].y;
                points.Add(rowPoints[0]);
                boundaryPoints.Add(points.Last());
                for (int i = 1; i < rowPoints.Count; i++)
                {
                    if (rowPoints[i].x - rowPoints[i - 1].x > 1)
                        for (int x = rowPoints[i - 1].x + 1; x <= rowPoints[i].x - 1; x++)
                        {
                            double t = (double)(x - rowPoints[i - 1].x) / (rowPoints[i].x - rowPoints[i - 1].x);
                            double z = t * (rowPoints[i].z - rowPoints[i - 1].z) + rowPoints[i - 1].z;
                            int red = (int)(t * (rowPoints[i].color.R - rowPoints[i - 1].color.R) +
                                             rowPoints[i - 1].color.R);
                            int green = (int)(t * (rowPoints[i].color.G - rowPoints[i - 1].color.G) +
                                               rowPoints[i - 1].color.G);
                            int blue = (int)(t * (rowPoints[i].color.B - rowPoints[i - 1].color.B) +
                                              rowPoints[i - 1].color.B);
                            Color color = Color.FromArgb(red, green, blue);
                            float UV_X =
                                Geometry.LinearInterpolation(rowPoints[i - 1].UV_X, rowPoints[i].UV_X, (float)t);
                            float UV_Y =
                                Geometry.LinearInterpolation(rowPoints[i - 1].UV_Y, rowPoints[i].UV_Y, (float)t);
                            points.Add(new Point2D(x, y, z, color, UV_X, UV_Y));
                        }
                    points.Add(rowPoints[i]);
                    boundaryPoints.Add(points.Last());
                }
                if (!boundaryColor.IsEmpty)
                    foreach (Point2D boundaryPoint in boundaryPoints)
                        boundaryPoint.color = boundaryColor;
            }

            //foreach (Point2D point in points)
            //{
            //    if (point.UV_X >= 0)
            //        System.Diagnostics.Debug.WriteLine(point);
            //}
            //System.Diagnostics.Debug.WriteLine('\n');
            return points;
        }

        protected override void Draw(List<List<Point2D>> rasterizedPrimitives, BitmapData bitmapData)
        {
            Clear(bitmapData);
            double[,] zBuffer = new double[bitmapData.Width, bitmapData.Height];
            for (int y = 0; y < bitmapData.Height; y++)
                for (int x = 0; x < bitmapData.Width; x++)
                    zBuffer[x, y] = double.MaxValue;
            foreach (List<Point2D> pointList in rasterizedPrimitives)
                foreach (Point2D point in pointList)
                    if (0 <= point.x && point.x < bitmapData.Width && 0 <= point.y && point.y < bitmapData.Height)
                        if (point.z < zBuffer[point.x, point.y])
                        {
                            if (point.UV_X < 0)
                            {
                                SetPixel(bitmapData, point.x, point.y, point.color);
                            }
                            else
                                SetPixel(bitmapData, point.x, point.y,
                                    texture.GetPixel(
                                        (int)(point.UV_X * (texture.Width - 1)),
                                        (int)(point.UV_Y * (texture.Height - 1))));
                            zBuffer[point.x, point.y] = point.z;
                        }
        }
    }
}
