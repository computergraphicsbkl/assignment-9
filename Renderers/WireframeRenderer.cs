﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Media.Media3D;

namespace Editor3D
{
    public class WireframeRenderer : RealtimeRenderer
    {
        public bool visibleOnly;

        public WireframeRenderer(IEnumerable<IPrimitive> primitives, Camera camera, Bitmap bitmap, Color ambientColor,
            bool visibleOnly)
            : base(primitives, camera, bitmap, ambientColor)
        {
            this.visibleOnly = visibleOnly;
        }

        protected override List<Point2D> RasterizeLine(Line line)
        {
            List<Point2D> points = new List<Point2D>();
            Point2D point1, point2;
            if ((int) line.A.X < (int) line.B.X)
            {
                point1 = (Point2D) line.A;
                point2 = (Point2D) line.B;
            }
            else
            {
                point1 = (Point2D) line.B;
                point2 = (Point2D) line.A;
            }
            int dx = point2.x - point1.x;
            int dy = point2.y - point1.y;
            double gradient;
            if (dx == 0 && dy > 0)
                gradient = 1;
            else if (dx == 0 && dy < 0)
                gradient = -1;
            else if (dx == 0 && dy == 0)
                return new List<Point2D>(new[] {point1});
            else
                gradient = (double)dy / dx;
            
            if (gradient >= 1)
            {
                int x = point1.x;
                int y = point1.y;
                double d = 2 * dx - dy;
                while (y <= point2.y)
                {
                    double t = (double) (y - point1.y) / (point2.y - point1.y);
                    double z = t * (point2.z - point1.z) + point1.z;
                    int red = (int)(t * (point2.color.R - point1.color.R) + point1.color.R);
                    int green = (int)(t * (point2.color.G - point1.color.G) + point1.color.G);
                    int blue = (int)(t * (point2.color.B - point1.color.B) + point1.color.B);
                    Color color = Color.FromArgb(red, green, blue);
                    if (z >= 0)
                        points.Add(new Point2D(x, y, z, color));
                    if (d < 0)
                        d += 2 * dx;
                    else
                    {
                        x++;
                        d += 2 * (dx - dy);
                    }
                    y++;
                }
            }
            else if (0 <= gradient && gradient < 1)
            {
                int x = point1.x;
                int y = point1.y;
                double d = 2 * dy - dx;
                while (x <= point2.x)
                {
                    double t = (double)(x - point1.x) / (point2.x - point1.x);
                    double z = t * (point2.z - point1.z) + point1.z;
                    int red = (int)(t * (point2.color.R - point1.color.R) + point1.color.R);
                    int green = (int)(t * (point2.color.G - point1.color.G) + point1.color.G);
                    int blue = (int)(t * (point2.color.B - point1.color.B) + point1.color.B);
                    Color color = Color.FromArgb(red, green, blue);
                    if (z >= 0)
                        points.Add(new Point2D(x, y, z, color));
                    if (d < 0)
                        d += 2 * dy;
                    else
                    {
                        y++;
                        d += 2 * (dy - dx);
                    }
                    x++;
                }
            }
            else if (-1 < gradient && gradient < 0)
            {
                dy = -dy;
                int x = point1.x;
                int y = point1.y;
                double d = 2 * dy - dx;
                while (x <= point2.x)
                {
                    double t = (double)(x - point1.x) / (point2.x - point1.x);
                    double z = t * (point2.z - point1.z) + point1.z;
                    int red = (int)(t * (point2.color.R - point1.color.R) + point1.color.R);
                    int green = (int)(t * (point2.color.G - point1.color.G) + point1.color.G);
                    int blue = (int)(t * (point2.color.B - point1.color.B) + point1.color.B);
                    Color color = Color.FromArgb(red, green, blue);
                    if (z >= 0)
                        points.Add(new Point2D(x, y, z, color));
                    if (d < 0)
                        d += 2 * dy;
                    else
                    {
                        y--;
                        d += 2 * (dy - dx);
                    }
                    x++;
                }
            }
            else
            {
                dy = -dy;
                int x = point1.x;
                int y = point1.y;
                double d = 2 * dx - dy;
                while (y >= point2.y)
                {
                    double t = (double) (y - point1.y) / (point2.y - point1.y);
                    double z = t * (point2.z - point1.z) + point1.z;
                    int red = (int)(t * (point2.color.R - point1.color.R) + point1.color.R);
                    int green = (int)(t * (point2.color.G - point1.color.G) + point1.color.G);
                    int blue = (int)(t * (point2.color.B - point1.color.B) + point1.color.B);
                    Color color = Color.FromArgb(red, green, blue);
                    if (z >= 0)
                        points.Add(new Point2D(x, y, z, color));
                    if (d < 0)
                        d += 2 * dx;
                    else
                    {
                        x++;
                        d += 2 * (dx - dy);
                    }
                    y--;
                }
            }

            return points;
        }
        
        protected override List<Point2D> RasterizeFacet(Facet facet)
        {
            List<Point2D> boundary = new List<Point2D>();
            for (int i = 0; i < facet.points.Count; i++)
                boundary.AddRange(RasterizeLine(new Line(
                    facet.points[i],
                    facet.points[(i + 1) % facet.points.Count])));
            return boundary;
        }

        protected override List<Point2D> RasterizePolyhedron(Polyhedron transformedPolyhedron)
        {
            List<Point2D> points = new List<Point2D>();
            Vector3D directionVector = new Vector3D(0, 0, 1);
            foreach (Facet polyhedronFacet in transformedPolyhedron.facets)
            {
                Vector3D normal = Geometry.NormalVector(polyhedronFacet, transformedPolyhedron);
                if (Vector3D.DotProduct(directionVector, normal) < 0
                    || !visibleOnly
                    || transformedPolyhedron.name == "Plot")
                    points.AddRange(RasterizeFacet(polyhedronFacet));
            }
            return points;
        }

        protected override List<Point2D> RasterizeLight(Light light)
        {
            List<Point2D> points = new List<Point2D>();
            points.AddRange(RasterizeLine(new Line(
                new Point3D(light.X - 10, light.Y, 0, light.Color),
                new Point3D(light.X + 10, light.Y, 0, light.Color))));
            points.AddRange(RasterizeLine(new Line(
                new Point3D(light.X - 7, light.Y - 7, 0, light.Color),
                new Point3D(light.X + 7, light.Y + 7, 0, light.Color))));
            points.AddRange(RasterizeLine(new Line(
                new Point3D(light.X, light.Y - 10, 0, light.Color),
                new Point3D(light.X, light.Y + 10, 0, light.Color))));
            points.AddRange(RasterizeLine(new Line(
                new Point3D(light.X + 7, light.Y - 7, 0, light.Color),
                new Point3D(light.X - 7, light.Y + 7, 0, light.Color))));
            return points;
        }
    }
}