﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Editor3D
{
    partial class MainForm
    {
        private const double addingPolyhedronSize = 0.5;
        private static Color defaultPolyhedronColor = Color.LightGray;
        
        private PlotForm plotForm = new PlotForm();
        private RotationFigureForm rotationFigureForm = new RotationFigureForm();

        private int currentWidth = 0;
        private int currentHeight = 0;
        private Camera camera;
        private Bitmap bitmap;
        private Renderer renderer;
        private Color ambientColor = Color.LightGray;
        private Color boundaryColor = Color.Black;
        
        private bool lockCameraEventHandlers = false;
        
        private void InitializeScene()
        {
            primitivesListBox.Items.Clear();
            
            Point3D oPoint = new Point3D(0, 0, 0, Color.Gray);
            Point3D xPoint = new Point3D(1, 0, 0, Color.Red);
            Line xAxis = new Line(oPoint, xPoint);
            Point3D yPoint = new Point3D(0, 1, 0, Color.LawnGreen);
            Line yAxis = new Line(oPoint, yPoint);
            Point3D zPoint = new Point3D(0, 0, 1, Color.Blue);
            Line zAxis = new Line(oPoint, zPoint);
            
            primitivesListBox.Items.AddRange(new object[] { xAxis, yAxis, zAxis });
        }
        
        private void InitializeCamera()
        {
            lockCameraEventHandlers = true;
            double xPosition = (double) cameraPositionXNumericUpDown.Value;
            double yPosition = (double) cameraPositionYNumericUpDown.Value;
            double zPosition = (double) cameraPositionZNumericUpDown.Value;
            double xRotation = (double) cameraRotationXNumericUpDown.Value * Math.PI / 180;
            double yRotation = (double) cameraRotationYNumericUpDown.Value * Math.PI / 180;
            double zRotation = (double) cameraRotationZNumericUpDown.Value * Math.PI / 180;
            double fieldOfView = (double) fovNumericUpDown.Value;
            camera = new Camera(new Point3D(xPosition, yPosition, zPosition),
                                new Point3D(xRotation, yRotation, zRotation),
                                fieldOfView);
            cameraDirectionXNumericUpDown.Value = (decimal) camera.Direction.X;
            cameraDirectionYNumericUpDown.Value = (decimal) camera.Direction.Y;
            cameraDirectionZNumericUpDown.Value = (decimal) camera.Direction.Z;
            cameraRollNumericUpDown.Value = (decimal) (camera.Roll * 180 / Math.PI);
            lockCameraEventHandlers = false;
        }

        private void InitializeRenderer()
        {
            renderer = new WireframeRenderer(primitivesListBox.Items.Cast<IPrimitive>(), camera, bitmap, ambientColor,
                visibleOnlyCheckBox.Checked);
            SetRenderMode();
        }
        
        private void InitializeInterface()
        {
            SetButtonColorImage(colorButton, Color.Empty);
            removeButton.Enabled = false;
            colorButton.Enabled = false;
            transformationsGroupBox.Enabled = false;
            SetRenderMode();
            SetButtonColorImage(ambientColorButton, renderer.ambientColor);
            if (renderer is SolidRenderer solidRenderer)
                SetButtonColorImage(boundaryColorButton, solidRenderer.boundaryColor);
        }

        private void SetRenderMode()
        {
            if (renderModeComboBox.SelectedItem == null)
                return;
            switch (renderModeComboBox.SelectedItem.ToString())
            {
                case "Каркасный":
                    renderer = new WireframeRenderer(primitivesListBox.Items.Cast<IPrimitive>(), camera, bitmap,
                        ambientColor, visibleOnlyCheckBox.Checked);
                    break;
                case "Сплошной":
                    renderer = new SolidRenderer(primitivesListBox.Items.Cast<IPrimitive>(), camera, bitmap,
                        ambientColor, visibleOnlyCheckBox.Checked, boundaryColor);
                    break;
                case "Шэйдинг":
                    renderer = new GouraudRenderer(primitivesListBox.Items.Cast<IPrimitive>(), camera, bitmap,
                        ambientColor, visibleOnlyCheckBox.Checked, boundaryColor);
                    break;
                case "Текстурирование":
                    renderer = new Renderers.TextureRenderer(primitivesListBox.Items.Cast<IPrimitive>(), camera, bitmap,
                        ambientColor, visibleOnlyCheckBox.Checked, boundaryColor);
                    break;
                case "Плавающий горизонт":
                    if (plotForm.ShowDialog() != DialogResult.OK)
                        return;
                    string function = plotForm.textBox1.Text;
                    double minX = (double) plotForm.numericUpDown1.Value;
                    double maxX = (double) plotForm.numericUpDown2.Value;
                    double minY = (double) plotForm.numericUpDown3.Value;
                    double maxY = (double) plotForm.numericUpDown4.Value;
                    int xTicks = (int) plotForm.numericUpDown5.Value;
					int yTicks = (int)plotForm.numericUpDown6.Value;
					double rotationY = (double)plotRotationYNumericUpDown.Value;
                    renderer = new FloatingHorizonRenderer(function, camera, bitmap,
                        ambientColor, minX, maxX, minY, maxY, xTicks, yTicks, Color.DarkBlue, Color.Blue, rotationY);
                    break;
            }
            if (renderer.GetType() == typeof(WireframeRenderer))
            {
                boundaryColorLabel.Enabled = false;
                boundaryColorButton.Enabled = false;
            }
            else
            {
                boundaryColorLabel.Enabled = true;
                boundaryColorButton.Enabled = true;
            }
        }

        private void Render()
        {
            if (renderer == null)
                return;
            if (renderPictureBox.Width != currentWidth || renderPictureBox.Height != currentHeight)
            {
                currentWidth = renderPictureBox.Width;
                currentHeight = renderPictureBox.Height;
                bitmap = new Bitmap(currentWidth, currentHeight);
                renderer.bitmap = bitmap;
            }
            renderer.Render();
            renderPictureBox.Image = bitmap;
            renderPictureBox.Refresh();
        }

        private void SetButtonColorImage(Button button, Color color)
        {
            button.Image = new Bitmap(10, 10);
            Graphics.FromImage(button.Image).Clear(color);
        }

        private void AddTetrahedron()
        {
            primitivesListBox.Items.Add(Polyhedron.Tetrahedron(addingPolyhedronSize, defaultPolyhedronColor));
        }

        private void AddIcosahedron()
        {
            primitivesListBox.Items.Add(Polyhedron.Icosahedron(addingPolyhedronSize, defaultPolyhedronColor));
        }

        private void AddDodecahedron()
        {
            primitivesListBox.Items.Add(Polyhedron.Dodecahedron(addingPolyhedronSize, defaultPolyhedronColor));
        }

        private void AddPlot()
        {
            if (plotForm.ShowDialog() != DialogResult.OK)
                return;
            string function = plotForm.textBox1.Text;
            double minX = (double) plotForm.numericUpDown1.Value;
            double maxX = (double) plotForm.numericUpDown2.Value;
            double minY = (double) plotForm.numericUpDown3.Value;
            double maxY = (double) plotForm.numericUpDown4.Value;
            int xTicks = (int) plotForm.numericUpDown5.Value;
            int yTicks = (int) plotForm.numericUpDown6.Value;
            primitivesListBox.Items.Add(Polyhedron.Plot(function, minX, maxX, minY, maxY, xTicks, yTicks,
                defaultPolyhedronColor));
        }

        private void AddRotationFigure()
        {
            if (rotationFigureForm.ShowDialog() != DialogResult.OK)
                return;
            List<Point3D> points = new List<Point3D>();
            string[] lines = rotationFigureForm.generatrixTextBox.Text
                .Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            foreach (string line in lines)
            {
                string[] strValues = line.Split();
                double x = Convert.ToDouble(strValues[0]);
                double y = Convert.ToDouble(strValues[1]);
                double z = Convert.ToDouble(strValues[2]);
                points.Add(new Point3D(x, y, z));
            }
            int splits = (int) rotationFigureForm.splitsNumericUpDown.Value;
            double angle = (360d / splits) * Math.PI / 180;
            Transformation matrix = Transformation.RotateX(angle);
            if (rotationFigureForm.yRadioButton.Checked)
                matrix = Transformation.RotateY(angle);
            else if (rotationFigureForm.zRadioButton.Checked)
                matrix = Transformation.RotateZ(angle);
            primitivesListBox.Items.Add(Polyhedron.RotationFigure(points, matrix, splits, defaultPolyhedronColor));
        }

        private void AddLight()
        {
            primitivesListBox.Items.Add(new Light(0, 0, 0, 10));
        }

        private void Translate()
        {
            double dx = (double)translateXNumericUpDown.Value;
            double dy = (double)translateYNumericUpDown.Value;
            double dz = (double)translateZNumericUpDown.Value;
            ((IPrimitive) primitivesListBox.SelectedItem).Apply(Transformation.Translate(dx, dy, dz));
        }

        private void Rotate()
        {
            double angleX = (double)rotateXNumericUpDown.Value * Math.PI / 180;
            double angleY = (double)rotateYNumericUpDown.Value * Math.PI / 180;
            double angleZ = (double)rotateZNumericUpDown.Value * Math.PI / 180;
            ((IPrimitive) primitivesListBox.SelectedItem).Apply(Transformation.RotateX(angleX));
            ((IPrimitive) primitivesListBox.SelectedItem).Apply(Transformation.RotateY(angleY));
            ((IPrimitive) primitivesListBox.SelectedItem).Apply(Transformation.RotateZ(angleZ));
        }

        private void Scale()
        {
            double scaleX = (double)scaleXNumericUpDown.Value;
            double scaleY = (double)scaleYNumericUpDown.Value;
            double scaleZ = (double)scaleZnNumericUpDown.Value;
            ((IPrimitive) primitivesListBox.SelectedItem).Apply(Transformation.Scale(scaleX, scaleY, scaleZ));
        }

        private void Reflect()
        {
            if (reflectXRadioButton.Checked)
                ((IPrimitive) primitivesListBox.SelectedItem).Apply(Transformation.ReflectX());
            else if (reflectYRadioButton.Checked)
                ((IPrimitive) primitivesListBox.SelectedItem).Apply(Transformation.ReflectY());
            else if (reflectZRadioButton.Checked)
                ((IPrimitive) primitivesListBox.SelectedItem).Apply(Transformation.ReflectZ());
        }

        private void CenterScale()
        {
            Point3D center = ((Polyhedron) primitivesListBox.SelectedItem).Center;
            double scaleX = (double)centerScaleXNumericUpDown.Value;
            double scaleY = (double)centerScaleYNumericUpDown.Value;
            double scaleZ = (double)centerScaleZNumericUpDown.Value;
            ((Polyhedron) primitivesListBox.SelectedItem).Apply(Transformation.Translate(-center.X, -center.Y, -center.Z));
            ((Polyhedron) primitivesListBox.SelectedItem).Apply(Transformation.Scale(scaleX, scaleY, scaleZ));
            ((Polyhedron) primitivesListBox.SelectedItem).Apply(Transformation.Translate(center.X, center.Y, center.Z));
        }

        private void CenterRotate()
        {
            Point3D center = ((Polyhedron) primitivesListBox.SelectedItem).Center;
            double angle = (double)centerRotateAngleNumericUpDown.Value * Math.PI / 180;
            ((Polyhedron) primitivesListBox.SelectedItem).Apply(Transformation.Translate(-center.X, -center.Y, -center.Z));
            if (centerRotateXRadioButton.Checked)
                ((Polyhedron) primitivesListBox.SelectedItem).Apply(Transformation.RotateX(angle));
            else if (centerRotateYRadioButton.Checked)
                ((Polyhedron) primitivesListBox.SelectedItem).Apply(Transformation.RotateY(angle));
            else if (centerRotateZRadioButton.Checked)
                ((Polyhedron) primitivesListBox.SelectedItem).Apply(Transformation.RotateZ(angle));
            ((Polyhedron) primitivesListBox.SelectedItem).Apply(Transformation.Translate(center.X, center.Y, center.Z));
        }

        private void LineRotate()
        {
            Line line = new Line(
                    new Point3D((double)point1XNumericUpDown.Value,
                        (double)point1YNumericUpDown.Value,
                        (double)point1ZNumericUpDown.Value),
                    new Point3D((double)point2XNumericUpDown.Value,
                        (double)point2YNumericUpDown.Value,
                        (double)point2ZNumericUpDown.Value));
                    Transformation translate = Transformation.Translate(-line.A.X, -line.A.Y, -line.A.Z);
                    Transformation reverseTranslate = Transformation.Translate(line.A.X, line.A.Y, line.A.Z);
                    ((IPrimitive) primitivesListBox.SelectedItem).Apply(translate);
                    line.Apply(translate);
                    // Исключить случай, когда линия лежит на оси Oy.
                    bool isOnOy = Math.Abs(line.B.X) <= double.Epsilon && Math.Abs(line.B.Z) <= double.Epsilon;
                    Transformation reverseRotateY = new Transformation();
                    Transformation reverseRotateZ = new Transformation();
                    if (!isOnOy)
                    {
                        double lineProjectionLength = Math.Sqrt(line.B.X * line.B.X + line.B.Z * line.B.Z);
                        double sin = line.B.Z / lineProjectionLength;
                        double cos = line.B.X / lineProjectionLength;
                        double lineProjectionAngle = Geometry.GetAngle(sin, cos);
                        Transformation rotateY = Transformation.RotateY(-lineProjectionAngle);
                        reverseRotateY = Transformation.RotateY(lineProjectionAngle);
                        ((IPrimitive) primitivesListBox.SelectedItem).Apply(rotateY);
                        line.Apply(rotateY);
                        lineProjectionLength = Math.Sqrt(line.B.X * line.B.X + line.B.Y * line.B.Y);
                        sin = line.B.X / lineProjectionLength;
                        cos = line.B.Y / lineProjectionLength;
                        lineProjectionAngle = Geometry.GetAngle(sin, cos);
                        Transformation rotateZ = Transformation.RotateZ(-lineProjectionAngle);
                        reverseRotateZ = Transformation.RotateZ(lineProjectionAngle);
                        ((IPrimitive) primitivesListBox.SelectedItem).Apply(rotateZ);
                        line.Apply(rotateZ);
                    }
                    double angle = (double)lineRotateAngleNumericUpDown.Value * Math.PI / 180;
                    Transformation rotate = Transformation.RotateY(angle);
                    ((IPrimitive) primitivesListBox.SelectedItem).Apply(rotate);
                    if (!isOnOy)
                    {
                        ((IPrimitive) primitivesListBox.SelectedItem).Apply(reverseRotateZ * reverseRotateY);
                        line.Apply(reverseRotateZ * reverseRotateY);
                    }
                    ((IPrimitive) primitivesListBox.SelectedItem).Apply(reverseTranslate);
                    line.Apply(reverseTranslate);
        }

        private void SetCameraPosition()
        {
            lockCameraEventHandlers = true;
            camera.position.X = (double) cameraPositionXNumericUpDown.Value;
            camera.position.Y = (double) cameraPositionYNumericUpDown.Value;
            camera.position.Z = (double) cameraPositionZNumericUpDown.Value;
            lockCameraEventHandlers = false;
        }
        
        private void SetCameraRotation()
        {
            lockCameraEventHandlers = true;
            camera.rotation.X = (double) cameraRotationXNumericUpDown.Value * Math.PI / 180;
            camera.rotation.Y = (double) cameraRotationYNumericUpDown.Value * Math.PI / 180;
            camera.rotation.Z = (double) cameraRotationZNumericUpDown.Value * Math.PI / 180;
            cameraDirectionXNumericUpDown.Value = (decimal) camera.Direction.X;
            cameraDirectionYNumericUpDown.Value = (decimal) camera.Direction.Y;
            cameraDirectionZNumericUpDown.Value = (decimal) camera.Direction.Z;
            cameraRollNumericUpDown.Value = (decimal) (camera.Roll * 180 / Math.PI);
            lockCameraEventHandlers = false;
        }

        private void SetCameraDirectionRoll()
        {
            lockCameraEventHandlers = true;
            camera.Direction = new Point3D(
                (double) cameraDirectionXNumericUpDown.Value,
                (double) cameraDirectionYNumericUpDown.Value,
                (double) cameraDirectionZNumericUpDown.Value);
            camera.Roll = (double) cameraRollNumericUpDown.Value * Math.PI / 180;
            cameraRotationXNumericUpDown.Value = (decimal) (camera.rotation.X * 180 / Math.PI);
            cameraRotationYNumericUpDown.Value = (decimal) (camera.rotation.Y * 180 / Math.PI);
            cameraRotationZNumericUpDown.Value = (decimal) (camera.rotation.Z * 180 / Math.PI);
            lockCameraEventHandlers = false;
        }

        private void PointRotateCamera()
        {
            lockCameraEventHandlers = true;
            Point3D rotationPoint = new Point3D(
                (double) cameraPointRotateXNumericUpDown.Value,
                (double) cameraPointRotateYNumericUpDown.Value,
                (double) cameraPointRotateZNumericUpDown.Value);
            double angle = (double) cameraPointRotateAngleNumericUpDown.Value * Math.PI / 180;
            camera.position.Apply(Transformation.Translate(-rotationPoint.X, -rotationPoint.Y, -rotationPoint.Z));
            if (cameraPointRotateXRadioButton.Checked)
            {
                camera.position.Apply(Transformation.RotateY(-camera.rotation.Y)
                    * Transformation.RotateX(angle)
                    * Transformation.RotateY(camera.rotation.Y));
                camera.rotation.X += angle;
                camera.rotation.X = (camera.rotation.X + 3 * Math.PI) % (2 * Math.PI) - Math.PI;
            }
            else if (cameraPointRotateYRadioButton.Checked)
            {
                camera.position.Apply(Transformation.RotateY(angle));
                camera.rotation.Y += angle;
                camera.rotation.Y = (camera.rotation.Y + 3 * Math.PI) % (2 * Math.PI) - Math.PI;
            }
            else
            {
                camera.position.Apply(Transformation.RotateY(-camera.rotation.Y)
                                      * Transformation.RotateZ(angle)
                                      * Transformation.RotateY(camera.rotation.Y));
                camera.rotation.Z += angle;
                camera.rotation.Z = (camera.rotation.Z + 3 * Math.PI) % (2 * Math.PI) - Math.PI;
            }
            camera.position.Apply(Transformation.Translate(rotationPoint.X, rotationPoint.Y, rotationPoint.Z));
            cameraPositionXNumericUpDown.Value = (decimal) camera.position.X;
            cameraPositionYNumericUpDown.Value = (decimal) camera.position.Y;
            cameraPositionZNumericUpDown.Value = (decimal) camera.position.Z;
            cameraRotationXNumericUpDown.Value = (decimal) (camera.rotation.X * 180 / Math.PI);
            cameraRotationYNumericUpDown.Value = (decimal) (camera.rotation.Y * 180 / Math.PI);
            cameraRotationZNumericUpDown.Value = (decimal) (camera.rotation.Z * 180 / Math.PI);
            cameraDirectionXNumericUpDown.Value = (decimal) camera.Direction.X;
            cameraDirectionYNumericUpDown.Value = (decimal) camera.Direction.Y;
            cameraDirectionZNumericUpDown.Value = (decimal) camera.Direction.Z;
            cameraRollNumericUpDown.Value = (decimal) (camera.Roll * 180 / Math.PI);
            lockCameraEventHandlers = false;
        }
    }
}