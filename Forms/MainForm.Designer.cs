﻿using System.Windows.Forms;

namespace Editor3D
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.label10 = new System.Windows.Forms.Label();
			this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.сценаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.tetrahedronToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.icosahedronToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.dodecahedronToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.plotToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.rotationFigureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
			this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
			this.numericUpDown25 = new System.Windows.Forms.NumericUpDown();
			this.label14 = new System.Windows.Forms.Label();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.numericUpDown24 = new System.Windows.Forms.NumericUpDown();
			this.button9 = new System.Windows.Forms.Button();
			this.button8 = new System.Windows.Forms.Button();
			this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
			this.colorDialog = new System.Windows.Forms.ColorDialog();
			this.mainTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.renderPictureBox = new System.Windows.Forms.PictureBox();
			this.sideTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.sceneGroupBox = new System.Windows.Forms.GroupBox();
			this.clearButton = new System.Windows.Forms.Button();
			this.removeButton = new System.Windows.Forms.Button();
			this.colorButton = new System.Windows.Forms.Button();
			this.colorLabel = new System.Windows.Forms.Label();
			this.lightButton = new System.Windows.Forms.Button();
			this.rotationFigureButton = new System.Windows.Forms.Button();
			this.plotButton = new System.Windows.Forms.Button();
			this.dodecahedronButton = new System.Windows.Forms.Button();
			this.icosahedronButton = new System.Windows.Forms.Button();
			this.tetrahedronButton = new System.Windows.Forms.Button();
			this.primitivesListBox = new System.Windows.Forms.ListBox();
			this.transformationsGroupBox = new System.Windows.Forms.GroupBox();
			this.transformationsTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.point2XNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.centerRotateButton = new System.Windows.Forms.Button();
			this.reflectButton = new System.Windows.Forms.Button();
			this.point1XNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.point1ZNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.point2ZNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.point1YNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.point2YNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.reflectLabel = new System.Windows.Forms.Label();
			this.translateLabel = new System.Windows.Forms.Label();
			this.scaleZnNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.scaleLabel = new System.Windows.Forms.Label();
			this.angleLabel = new System.Windows.Forms.Label();
			this.scaleYNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.scaleXNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.centerRotateAngleNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.xLabel = new System.Windows.Forms.Label();
			this.yLabel = new System.Windows.Forms.Label();
			this.zLabel = new System.Windows.Forms.Label();
			this.rotateZNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.rotateYNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.rotateXNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.rotateLabel = new System.Windows.Forms.Label();
			this.translateXNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.translateYNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.translateZNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.translateButton = new System.Windows.Forms.Button();
			this.rotateButton = new System.Windows.Forms.Button();
			this.scaleButton = new System.Windows.Forms.Button();
			this.centerScaleLabel = new System.Windows.Forms.Label();
			this.centerScaleButton = new System.Windows.Forms.Button();
			this.centerRotateLabel = new System.Windows.Forms.Label();
			this.centerScaleYNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.centerScaleXNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.centerScaleZNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.panel1 = new System.Windows.Forms.Panel();
			this.reflectXRadioButton = new System.Windows.Forms.RadioButton();
			this.reflectYRadioButton = new System.Windows.Forms.RadioButton();
			this.reflectZRadioButton = new System.Windows.Forms.RadioButton();
			this.panel2 = new System.Windows.Forms.Panel();
			this.centerRotateXRadioButton = new System.Windows.Forms.RadioButton();
			this.centerRotateYRadioButton = new System.Windows.Forms.RadioButton();
			this.centerRotateZRadioButton = new System.Windows.Forms.RadioButton();
			this.lineRotateButton = new System.Windows.Forms.Button();
			this.lineRotateLabel = new System.Windows.Forms.Label();
			this.lineRotateAngleNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.cameraGroupBox = new System.Windows.Forms.GroupBox();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.panel3 = new System.Windows.Forms.Panel();
			this.cameraPointRotateXRadioButton = new System.Windows.Forms.RadioButton();
			this.cameraPointRotateYRadioButton = new System.Windows.Forms.RadioButton();
			this.cameraPointRotateZRadioButton = new System.Windows.Forms.RadioButton();
			this.cameraXLabel = new System.Windows.Forms.Label();
			this.cameraYLabel = new System.Windows.Forms.Label();
			this.cameraZLabel = new System.Windows.Forms.Label();
			this.cameraAngleLabel = new System.Windows.Forms.Label();
			this.cameraPositionXNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.cameraPositionYNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.cameraPositionZNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.cameraPositionLabel = new System.Windows.Forms.Label();
			this.cameraDirectionZNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.cameraDirectionYNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.cameraDirectionXNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.cameraDirectionLabel = new System.Windows.Forms.Label();
			this.cameraPointRotateLabel = new System.Windows.Forms.Label();
			this.cameraPointRotateXNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.cameraPointRotateYNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.cameraPointRotateZNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.fovLabel = new System.Windows.Forms.Label();
			this.fovNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.cameraPointRotateButton = new System.Windows.Forms.Button();
			this.cameraPointRotateAngleNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.cameraRollNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.cameraRollLabel = new System.Windows.Forms.Label();
			this.cameraRotationLabel = new System.Windows.Forms.Label();
			this.cameraRotationXNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.cameraRotationYNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.cameraRotationZNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.renderingGroupBox = new System.Windows.Forms.GroupBox();
			this.visibleOnlyCheckBox = new System.Windows.Forms.CheckBox();
			this.renderModeLabel = new System.Windows.Forms.Label();
			this.renderModeComboBox = new System.Windows.Forms.ComboBox();
			this.boundaryColorButton = new System.Windows.Forms.Button();
			this.ambientColorButton = new System.Windows.Forms.Button();
			this.boundaryColorLabel = new System.Windows.Forms.Label();
			this.ambientColorLabel = new System.Windows.Forms.Label();
			this.plotRotationYNumericUpDown = new System.Windows.Forms.NumericUpDown();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
			this.mainMenuStrip.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown25)).BeginInit();
			this.groupBox3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown24)).BeginInit();
			this.tableLayoutPanel5.SuspendLayout();
			this.mainTableLayoutPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.renderPictureBox)).BeginInit();
			this.sideTableLayoutPanel.SuspendLayout();
			this.sceneGroupBox.SuspendLayout();
			this.transformationsGroupBox.SuspendLayout();
			this.transformationsTableLayoutPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.point2XNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.point1XNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.point1ZNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.point2ZNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.point1YNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.point2YNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.scaleZnNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.scaleYNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.scaleXNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.centerRotateAngleNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.rotateZNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.rotateYNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.rotateXNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.translateXNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.translateYNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.translateZNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.centerScaleYNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.centerScaleXNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.centerScaleZNumericUpDown)).BeginInit();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.lineRotateAngleNumericUpDown)).BeginInit();
			this.cameraGroupBox.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.panel3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cameraPositionXNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cameraPositionYNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cameraPositionZNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cameraDirectionZNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cameraDirectionYNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cameraDirectionXNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cameraPointRotateXNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cameraPointRotateYNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cameraPointRotateZNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fovNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cameraPointRotateAngleNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cameraRollNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cameraRotationXNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cameraRotationYNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cameraRotationZNumericUpDown)).BeginInit();
			this.renderingGroupBox.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.plotRotationYNumericUpDown)).BeginInit();
			this.SuspendLayout();
			// 
			// label10
			// 
			this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.label10.AutoSize = true;
			this.label10.BackColor = System.Drawing.SystemColors.Control;
			this.label10.Location = new System.Drawing.Point(3, 40);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(14, 20);
			this.label10.TabIndex = 0;
			this.label10.Text = "Перемещение";
			// 
			// numericUpDown3
			// 
			this.numericUpDown3.Location = new System.Drawing.Point(63, 83);
			this.numericUpDown3.Name = "numericUpDown3";
			this.numericUpDown3.Size = new System.Drawing.Size(14, 20);
			this.numericUpDown3.TabIndex = 0;
			// 
			// label5
			// 
			this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(3, 143);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(14, 91);
			this.label5.TabIndex = 0;
			this.label5.Text = "Масштаб";
			// 
			// mainMenuStrip
			// 
			this.mainMenuStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
			this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.сценаToolStripMenuItem});
			this.mainMenuStrip.Location = new System.Drawing.Point(0, 0);
			this.mainMenuStrip.Name = "mainMenuStrip";
			this.mainMenuStrip.Padding = new System.Windows.Forms.Padding(4, 1, 0, 1);
			this.mainMenuStrip.Size = new System.Drawing.Size(1087, 24);
			this.mainMenuStrip.TabIndex = 1;
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(48, 22);
			this.fileToolStripMenuItem.Text = "Файл";
			// 
			// newToolStripMenuItem
			// 
			this.newToolStripMenuItem.Name = "newToolStripMenuItem";
			this.newToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
			this.newToolStripMenuItem.Text = "Новый";
			this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
			// 
			// openToolStripMenuItem
			// 
			this.openToolStripMenuItem.Name = "openToolStripMenuItem";
			this.openToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
			this.openToolStripMenuItem.Text = "Открыть";
			this.openToolStripMenuItem.Click += new System.EventHandler(this.openFileToolStripMenuItem_Click);
			// 
			// saveToolStripMenuItem
			// 
			this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
			this.saveToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
			this.saveToolStripMenuItem.Text = "Сохранить";
			this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveFileToolStripMenuItem_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(129, 6);
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
			this.exitToolStripMenuItem.Text = "Выход";
			this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
			// 
			// сценаToolStripMenuItem
			// 
			this.сценаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem});
			this.сценаToolStripMenuItem.Name = "сценаToolStripMenuItem";
			this.сценаToolStripMenuItem.Size = new System.Drawing.Size(53, 22);
			this.сценаToolStripMenuItem.Text = "Сцена";
			// 
			// addToolStripMenuItem
			// 
			this.addToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tetrahedronToolStripMenuItem,
            this.icosahedronToolStripMenuItem,
            this.dodecahedronToolStripMenuItem,
            this.plotToolStripMenuItem,
            this.rotationFigureToolStripMenuItem});
			this.addToolStripMenuItem.Name = "addToolStripMenuItem";
			this.addToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
			this.addToolStripMenuItem.Text = "Добавить";
			// 
			// tetrahedronToolStripMenuItem
			// 
			this.tetrahedronToolStripMenuItem.Name = "tetrahedronToolStripMenuItem";
			this.tetrahedronToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
			this.tetrahedronToolStripMenuItem.Text = "Тетраэдр";
			this.tetrahedronToolStripMenuItem.Click += new System.EventHandler(this.tetrahedronToolStripMenuItem_Click);
			// 
			// icosahedronToolStripMenuItem
			// 
			this.icosahedronToolStripMenuItem.Name = "icosahedronToolStripMenuItem";
			this.icosahedronToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
			this.icosahedronToolStripMenuItem.Text = "Икосаэдр";
			this.icosahedronToolStripMenuItem.Click += new System.EventHandler(this.icosahedronToolStripMenuItem_Click);
			// 
			// dodecahedronToolStripMenuItem
			// 
			this.dodecahedronToolStripMenuItem.Name = "dodecahedronToolStripMenuItem";
			this.dodecahedronToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
			this.dodecahedronToolStripMenuItem.Text = "Додекаэдр";
			this.dodecahedronToolStripMenuItem.Click += new System.EventHandler(this.dodecahedronToolStripMenuItem_Click);
			// 
			// plotToolStripMenuItem
			// 
			this.plotToolStripMenuItem.Name = "plotToolStripMenuItem";
			this.plotToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
			this.plotToolStripMenuItem.Text = "График функции";
			this.plotToolStripMenuItem.Click += new System.EventHandler(this.plotToolStripMenuItem_Click);
			// 
			// rotationFigureToolStripMenuItem
			// 
			this.rotationFigureToolStripMenuItem.Name = "rotationFigureToolStripMenuItem";
			this.rotationFigureToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
			this.rotationFigureToolStripMenuItem.Text = "Фигура вращения";
			this.rotationFigureToolStripMenuItem.Click += new System.EventHandler(this.rotationFigureToolStripMenuItem_Click);
			// 
			// openFileDialog
			// 
			this.openFileDialog.DefaultExt = "obj";
			this.openFileDialog.FileName = "openFileDialog1";
			this.openFileDialog.Filter = "OBJ files|*.obj|All files|*.*";
			// 
			// saveFileDialog
			// 
			this.saveFileDialog.DefaultExt = "obj";
			this.saveFileDialog.Filter = "OBJ files|*.obj|All files|*.*";
			// 
			// tableLayoutPanel4
			// 
			this.tableLayoutPanel4.AutoSize = true;
			this.tableLayoutPanel4.ColumnCount = 6;
			this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel4.Name = "tableLayoutPanel4";
			this.tableLayoutPanel4.RowCount = 9;
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel4.Size = new System.Drawing.Size(200, 100);
			this.tableLayoutPanel4.TabIndex = 0;
			// 
			// numericUpDown25
			// 
			this.numericUpDown25.DecimalPlaces = 1;
			this.numericUpDown25.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.numericUpDown25.Location = new System.Drawing.Point(41, 42);
			this.numericUpDown25.Maximum = new decimal(new int[] {
            -1,
            -1,
            -1,
            0});
			this.numericUpDown25.Name = "numericUpDown25";
			this.numericUpDown25.Size = new System.Drawing.Size(50, 20);
			this.numericUpDown25.TabIndex = 1;
			this.numericUpDown25.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(6, 44);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(28, 13);
			this.label14.TabIndex = 0;
			this.label14.Text = "FOV";
			// 
			// groupBox3
			// 
			this.groupBox3.AutoSize = true;
			this.groupBox3.Controls.Add(this.label14);
			this.groupBox3.Controls.Add(this.numericUpDown25);
			this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox3.Location = new System.Drawing.Point(3, 17);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(1, 289);
			this.groupBox3.TabIndex = 0;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Проекция";
			// 
			// numericUpDown24
			// 
			this.numericUpDown24.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.numericUpDown24.Location = new System.Drawing.Point(3, 152);
			this.numericUpDown24.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
			this.numericUpDown24.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
			this.numericUpDown24.Name = "numericUpDown24";
			this.tableLayoutPanel5.SetRowSpan(this.numericUpDown24, 2);
			this.numericUpDown24.Size = new System.Drawing.Size(50, 20);
			this.numericUpDown24.TabIndex = 2;
			// 
			// button9
			// 
			this.button9.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.button9.AutoSize = true;
			this.button9.Location = new System.Drawing.Point(59, 147);
			this.button9.Name = "button9";
			this.tableLayoutPanel5.SetRowSpan(this.button9, 2);
			this.button9.Size = new System.Drawing.Size(95, 30);
			this.button9.TabIndex = 1;
			this.button9.Text = "Применить";
			this.button9.UseVisualStyleBackColor = true;
			// 
			// button8
			// 
			this.button8.AutoSize = true;
			this.button8.Location = new System.Drawing.Point(59, 3);
			this.button8.Name = "button8";
			this.button8.Size = new System.Drawing.Size(95, 30);
			this.button8.TabIndex = 0;
			this.button8.Text = "Применить";
			this.button8.UseVisualStyleBackColor = true;
			// 
			// tableLayoutPanel5
			// 
			this.tableLayoutPanel5.AutoSize = true;
			this.tableLayoutPanel5.ColumnCount = 6;
			this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel5.Controls.Add(this.button8, 5, 5);
			this.tableLayoutPanel5.Controls.Add(this.button9, 5, 7);
			this.tableLayoutPanel5.Controls.Add(this.numericUpDown24, 4, 7);
			this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 17);
			this.tableLayoutPanel5.Name = "tableLayoutPanel5";
			this.tableLayoutPanel5.RowCount = 9;
			this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21F));
			this.tableLayoutPanel5.Size = new System.Drawing.Size(1, 289);
			this.tableLayoutPanel5.TabIndex = 0;
			// 
			// mainTableLayoutPanel
			// 
			this.mainTableLayoutPanel.ColumnCount = 2;
			this.mainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.mainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.mainTableLayoutPanel.Controls.Add(this.renderPictureBox, 0, 0);
			this.mainTableLayoutPanel.Controls.Add(this.sideTableLayoutPanel, 1, 0);
			this.mainTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainTableLayoutPanel.Location = new System.Drawing.Point(0, 24);
			this.mainTableLayoutPanel.Name = "mainTableLayoutPanel";
			this.mainTableLayoutPanel.RowCount = 1;
			this.mainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.mainTableLayoutPanel.Size = new System.Drawing.Size(1087, 851);
			this.mainTableLayoutPanel.TabIndex = 0;
			// 
			// renderPictureBox
			// 
			this.renderPictureBox.BackColor = System.Drawing.Color.White;
			this.renderPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.renderPictureBox.Location = new System.Drawing.Point(3, 3);
			this.renderPictureBox.Name = "renderPictureBox";
			this.renderPictureBox.Size = new System.Drawing.Size(668, 845);
			this.renderPictureBox.TabIndex = 0;
			this.renderPictureBox.TabStop = false;
			this.renderPictureBox.SizeChanged += new System.EventHandler(this.renderPictureBox_SizeChanged);
			// 
			// sideTableLayoutPanel
			// 
			this.sideTableLayoutPanel.AutoSize = true;
			this.sideTableLayoutPanel.ColumnCount = 1;
			this.sideTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.sideTableLayoutPanel.Controls.Add(this.sceneGroupBox, 0, 0);
			this.sideTableLayoutPanel.Controls.Add(this.transformationsGroupBox, 0, 1);
			this.sideTableLayoutPanel.Controls.Add(this.cameraGroupBox, 0, 2);
			this.sideTableLayoutPanel.Controls.Add(this.renderingGroupBox, 0, 3);
			this.sideTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.sideTableLayoutPanel.Location = new System.Drawing.Point(677, 3);
			this.sideTableLayoutPanel.Name = "sideTableLayoutPanel";
			this.sideTableLayoutPanel.RowCount = 5;
			this.sideTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.sideTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.sideTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.sideTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.sideTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.sideTableLayoutPanel.Size = new System.Drawing.Size(407, 845);
			this.sideTableLayoutPanel.TabIndex = 1;
			// 
			// sceneGroupBox
			// 
			this.sceneGroupBox.AutoSize = true;
			this.sceneGroupBox.Controls.Add(this.clearButton);
			this.sceneGroupBox.Controls.Add(this.removeButton);
			this.sceneGroupBox.Controls.Add(this.colorButton);
			this.sceneGroupBox.Controls.Add(this.colorLabel);
			this.sceneGroupBox.Controls.Add(this.lightButton);
			this.sceneGroupBox.Controls.Add(this.rotationFigureButton);
			this.sceneGroupBox.Controls.Add(this.plotButton);
			this.sceneGroupBox.Controls.Add(this.dodecahedronButton);
			this.sceneGroupBox.Controls.Add(this.icosahedronButton);
			this.sceneGroupBox.Controls.Add(this.tetrahedronButton);
			this.sceneGroupBox.Controls.Add(this.primitivesListBox);
			this.sceneGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.sceneGroupBox.Location = new System.Drawing.Point(3, 3);
			this.sceneGroupBox.Name = "sceneGroupBox";
			this.sceneGroupBox.Size = new System.Drawing.Size(401, 241);
			this.sceneGroupBox.TabIndex = 0;
			this.sceneGroupBox.TabStop = false;
			this.sceneGroupBox.Text = "Сцена";
			// 
			// clearButton
			// 
			this.clearButton.AutoSize = true;
			this.clearButton.Location = new System.Drawing.Point(321, 186);
			this.clearButton.Name = "clearButton";
			this.clearButton.Size = new System.Drawing.Size(74, 26);
			this.clearButton.TabIndex = 0;
			this.clearButton.Text = "Очистить";
			this.clearButton.UseVisualStyleBackColor = true;
			this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
			// 
			// removeButton
			// 
			this.removeButton.AutoSize = true;
			this.removeButton.Location = new System.Drawing.Point(242, 186);
			this.removeButton.Name = "removeButton";
			this.removeButton.Size = new System.Drawing.Size(74, 26);
			this.removeButton.TabIndex = 1;
			this.removeButton.Text = "Удалить";
			this.removeButton.UseVisualStyleBackColor = true;
			this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
			// 
			// colorButton
			// 
			this.colorButton.AutoSize = true;
			this.colorButton.Location = new System.Drawing.Point(199, 186);
			this.colorButton.Name = "colorButton";
			this.colorButton.Size = new System.Drawing.Size(38, 26);
			this.colorButton.TabIndex = 2;
			this.colorButton.UseVisualStyleBackColor = true;
			this.colorButton.Click += new System.EventHandler(this.colorButton_Click);
			// 
			// colorLabel
			// 
			this.colorLabel.AutoSize = true;
			this.colorLabel.Location = new System.Drawing.Point(163, 193);
			this.colorLabel.Margin = new System.Windows.Forms.Padding(3, 0, 5, 0);
			this.colorLabel.Name = "colorLabel";
			this.colorLabel.Size = new System.Drawing.Size(32, 13);
			this.colorLabel.TabIndex = 3;
			this.colorLabel.Text = "Цвет";
			// 
			// lightButton
			// 
			this.lightButton.AutoSize = true;
			this.lightButton.Location = new System.Drawing.Point(5, 186);
			this.lightButton.Name = "lightButton";
			this.lightButton.Size = new System.Drawing.Size(74, 36);
			this.lightButton.TabIndex = 4;
			this.lightButton.Text = "Источник\r\nсвета";
			this.lightButton.UseVisualStyleBackColor = true;
			this.lightButton.Click += new System.EventHandler(this.lightButton_Click);
			// 
			// rotationFigureButton
			// 
			this.rotationFigureButton.AutoSize = true;
			this.rotationFigureButton.Location = new System.Drawing.Point(321, 145);
			this.rotationFigureButton.Name = "rotationFigureButton";
			this.rotationFigureButton.Size = new System.Drawing.Size(74, 36);
			this.rotationFigureButton.TabIndex = 4;
			this.rotationFigureButton.Text = "Фигура\r\nвращения";
			this.rotationFigureButton.UseVisualStyleBackColor = true;
			this.rotationFigureButton.Click += new System.EventHandler(this.rotationFigureButton_Click);
			// 
			// plotButton
			// 
			this.plotButton.AutoSize = true;
			this.plotButton.Location = new System.Drawing.Point(242, 145);
			this.plotButton.Name = "plotButton";
			this.plotButton.Size = new System.Drawing.Size(74, 36);
			this.plotButton.TabIndex = 5;
			this.plotButton.Text = "График\r\nфункции";
			this.plotButton.UseVisualStyleBackColor = true;
			this.plotButton.Click += new System.EventHandler(this.plotButton_Click);
			// 
			// dodecahedronButton
			// 
			this.dodecahedronButton.AutoSize = true;
			this.dodecahedronButton.Location = new System.Drawing.Point(163, 145);
			this.dodecahedronButton.Name = "dodecahedronButton";
			this.dodecahedronButton.Size = new System.Drawing.Size(74, 36);
			this.dodecahedronButton.TabIndex = 6;
			this.dodecahedronButton.Text = "Додекаэдр";
			this.dodecahedronButton.UseVisualStyleBackColor = true;
			this.dodecahedronButton.Click += new System.EventHandler(this.dodecahedronButton_Click);
			// 
			// icosahedronButton
			// 
			this.icosahedronButton.AutoSize = true;
			this.icosahedronButton.Location = new System.Drawing.Point(84, 145);
			this.icosahedronButton.Name = "icosahedronButton";
			this.icosahedronButton.Size = new System.Drawing.Size(74, 36);
			this.icosahedronButton.TabIndex = 7;
			this.icosahedronButton.Text = "Исосаэдр";
			this.icosahedronButton.UseVisualStyleBackColor = true;
			this.icosahedronButton.Click += new System.EventHandler(this.icosahedronButton_Click);
			// 
			// tetrahedronButton
			// 
			this.tetrahedronButton.AutoSize = true;
			this.tetrahedronButton.Location = new System.Drawing.Point(5, 145);
			this.tetrahedronButton.Name = "tetrahedronButton";
			this.tetrahedronButton.Size = new System.Drawing.Size(74, 36);
			this.tetrahedronButton.TabIndex = 8;
			this.tetrahedronButton.Text = "Тетраэдр";
			this.tetrahedronButton.UseVisualStyleBackColor = true;
			this.tetrahedronButton.Click += new System.EventHandler(this.tetrahedronButton_Click);
			// 
			// primitivesListBox
			// 
			this.primitivesListBox.FormattingEnabled = true;
			this.primitivesListBox.Location = new System.Drawing.Point(5, 19);
			this.primitivesListBox.Name = "primitivesListBox";
			this.primitivesListBox.Size = new System.Drawing.Size(390, 121);
			this.primitivesListBox.TabIndex = 9;
			this.primitivesListBox.SelectedIndexChanged += new System.EventHandler(this.primitivesListBox_SelectedIndexChanged);
			// 
			// transformationsGroupBox
			// 
			this.transformationsGroupBox.AutoSize = true;
			this.transformationsGroupBox.Controls.Add(this.transformationsTableLayoutPanel);
			this.transformationsGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.transformationsGroupBox.Location = new System.Drawing.Point(3, 250);
			this.transformationsGroupBox.Name = "transformationsGroupBox";
			this.transformationsGroupBox.Size = new System.Drawing.Size(401, 276);
			this.transformationsGroupBox.TabIndex = 2;
			this.transformationsGroupBox.TabStop = false;
			this.transformationsGroupBox.Text = "Преобразования";
			// 
			// transformationsTableLayoutPanel
			// 
			this.transformationsTableLayoutPanel.AutoSize = true;
			this.transformationsTableLayoutPanel.ColumnCount = 6;
			this.transformationsTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.transformationsTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.transformationsTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.transformationsTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.transformationsTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.transformationsTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.transformationsTableLayoutPanel.Controls.Add(this.point2XNumericUpDown, 0, 8);
			this.transformationsTableLayoutPanel.Controls.Add(this.centerRotateButton, 5, 6);
			this.transformationsTableLayoutPanel.Controls.Add(this.reflectButton, 5, 4);
			this.transformationsTableLayoutPanel.Controls.Add(this.point1XNumericUpDown, 1, 7);
			this.transformationsTableLayoutPanel.Controls.Add(this.point1ZNumericUpDown, 3, 7);
			this.transformationsTableLayoutPanel.Controls.Add(this.point2ZNumericUpDown, 2, 8);
			this.transformationsTableLayoutPanel.Controls.Add(this.point1YNumericUpDown, 2, 7);
			this.transformationsTableLayoutPanel.Controls.Add(this.point2YNumericUpDown, 1, 8);
			this.transformationsTableLayoutPanel.Controls.Add(this.reflectLabel, 0, 4);
			this.transformationsTableLayoutPanel.Controls.Add(this.translateLabel, 0, 1);
			this.transformationsTableLayoutPanel.Controls.Add(this.scaleZnNumericUpDown, 3, 3);
			this.transformationsTableLayoutPanel.Controls.Add(this.scaleLabel, 0, 3);
			this.transformationsTableLayoutPanel.Controls.Add(this.angleLabel, 4, 0);
			this.transformationsTableLayoutPanel.Controls.Add(this.scaleYNumericUpDown, 2, 3);
			this.transformationsTableLayoutPanel.Controls.Add(this.scaleXNumericUpDown, 1, 3);
			this.transformationsTableLayoutPanel.Controls.Add(this.centerRotateAngleNumericUpDown, 4, 6);
			this.transformationsTableLayoutPanel.Controls.Add(this.xLabel, 1, 0);
			this.transformationsTableLayoutPanel.Controls.Add(this.yLabel, 2, 0);
			this.transformationsTableLayoutPanel.Controls.Add(this.zLabel, 3, 0);
			this.transformationsTableLayoutPanel.Controls.Add(this.rotateZNumericUpDown, 3, 2);
			this.transformationsTableLayoutPanel.Controls.Add(this.rotateYNumericUpDown, 2, 2);
			this.transformationsTableLayoutPanel.Controls.Add(this.rotateXNumericUpDown, 1, 2);
			this.transformationsTableLayoutPanel.Controls.Add(this.rotateLabel, 0, 2);
			this.transformationsTableLayoutPanel.Controls.Add(this.translateXNumericUpDown, 1, 1);
			this.transformationsTableLayoutPanel.Controls.Add(this.translateYNumericUpDown, 2, 1);
			this.transformationsTableLayoutPanel.Controls.Add(this.translateZNumericUpDown, 3, 1);
			this.transformationsTableLayoutPanel.Controls.Add(this.translateButton, 5, 1);
			this.transformationsTableLayoutPanel.Controls.Add(this.rotateButton, 5, 2);
			this.transformationsTableLayoutPanel.Controls.Add(this.scaleButton, 5, 3);
			this.transformationsTableLayoutPanel.Controls.Add(this.centerScaleLabel, 0, 5);
			this.transformationsTableLayoutPanel.Controls.Add(this.centerScaleButton, 5, 5);
			this.transformationsTableLayoutPanel.Controls.Add(this.centerRotateLabel, 0, 6);
			this.transformationsTableLayoutPanel.Controls.Add(this.centerScaleYNumericUpDown, 2, 5);
			this.transformationsTableLayoutPanel.Controls.Add(this.centerScaleXNumericUpDown, 1, 5);
			this.transformationsTableLayoutPanel.Controls.Add(this.centerScaleZNumericUpDown, 3, 5);
			this.transformationsTableLayoutPanel.Controls.Add(this.panel1, 1, 4);
			this.transformationsTableLayoutPanel.Controls.Add(this.panel2, 1, 6);
			this.transformationsTableLayoutPanel.Controls.Add(this.lineRotateButton, 5, 7);
			this.transformationsTableLayoutPanel.Controls.Add(this.lineRotateLabel, 0, 7);
			this.transformationsTableLayoutPanel.Controls.Add(this.lineRotateAngleNumericUpDown, 4, 7);
			this.transformationsTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.transformationsTableLayoutPanel.Location = new System.Drawing.Point(3, 16);
			this.transformationsTableLayoutPanel.Name = "transformationsTableLayoutPanel";
			this.transformationsTableLayoutPanel.RowCount = 9;
			this.transformationsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.transformationsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.transformationsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.transformationsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.transformationsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.transformationsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.transformationsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.transformationsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.transformationsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.transformationsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18F));
			this.transformationsTableLayoutPanel.Size = new System.Drawing.Size(395, 257);
			this.transformationsTableLayoutPanel.TabIndex = 0;
			// 
			// point2XNumericUpDown
			// 
			this.point2XNumericUpDown.Location = new System.Drawing.Point(115, 234);
			this.point2XNumericUpDown.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
			this.point2XNumericUpDown.Name = "point2XNumericUpDown";
			this.point2XNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.point2XNumericUpDown.TabIndex = 0;
			// 
			// centerRotateButton
			// 
			this.centerRotateButton.AutoSize = true;
			this.centerRotateButton.Location = new System.Drawing.Point(311, 176);
			this.centerRotateButton.Name = "centerRotateButton";
			this.centerRotateButton.Size = new System.Drawing.Size(81, 26);
			this.centerRotateButton.TabIndex = 1;
			this.centerRotateButton.Text = "Применить";
			this.centerRotateButton.UseVisualStyleBackColor = true;
			this.centerRotateButton.Click += new System.EventHandler(this.centerRotateButton_Click);
			// 
			// reflectButton
			// 
			this.reflectButton.AutoSize = true;
			this.reflectButton.Location = new System.Drawing.Point(311, 112);
			this.reflectButton.Name = "reflectButton";
			this.reflectButton.Size = new System.Drawing.Size(81, 26);
			this.reflectButton.TabIndex = 2;
			this.reflectButton.Text = "Применить";
			this.reflectButton.UseVisualStyleBackColor = true;
			this.reflectButton.Click += new System.EventHandler(this.reflectButton_Click);
			// 
			// point1XNumericUpDown
			// 
			this.point1XNumericUpDown.Location = new System.Drawing.Point(115, 208);
			this.point1XNumericUpDown.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
			this.point1XNumericUpDown.Name = "point1XNumericUpDown";
			this.point1XNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.point1XNumericUpDown.TabIndex = 3;
			// 
			// point1ZNumericUpDown
			// 
			this.point1ZNumericUpDown.Location = new System.Drawing.Point(213, 208);
			this.point1ZNumericUpDown.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
			this.point1ZNumericUpDown.Name = "point1ZNumericUpDown";
			this.point1ZNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.point1ZNumericUpDown.TabIndex = 4;
			// 
			// point2ZNumericUpDown
			// 
			this.point2ZNumericUpDown.Location = new System.Drawing.Point(213, 234);
			this.point2ZNumericUpDown.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
			this.point2ZNumericUpDown.Name = "point2ZNumericUpDown";
			this.point2ZNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.point2ZNumericUpDown.TabIndex = 5;
			// 
			// point1YNumericUpDown
			// 
			this.point1YNumericUpDown.Location = new System.Drawing.Point(164, 208);
			this.point1YNumericUpDown.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
			this.point1YNumericUpDown.Name = "point1YNumericUpDown";
			this.point1YNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.point1YNumericUpDown.TabIndex = 6;
			// 
			// point2YNumericUpDown
			// 
			this.point2YNumericUpDown.Location = new System.Drawing.Point(164, 234);
			this.point2YNumericUpDown.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
			this.point2YNumericUpDown.Name = "point2YNumericUpDown";
			this.point2YNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.point2YNumericUpDown.TabIndex = 7;
			// 
			// reflectLabel
			// 
			this.reflectLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.reflectLabel.AutoSize = true;
			this.reflectLabel.Location = new System.Drawing.Point(24, 118);
			this.reflectLabel.Name = "reflectLabel";
			this.reflectLabel.Size = new System.Drawing.Size(64, 13);
			this.reflectLabel.TabIndex = 8;
			this.reflectLabel.Text = "Отражение";
			// 
			// translateLabel
			// 
			this.translateLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.translateLabel.AutoSize = true;
			this.translateLabel.BackColor = System.Drawing.SystemColors.Control;
			this.translateLabel.Location = new System.Drawing.Point(16, 22);
			this.translateLabel.Name = "translateLabel";
			this.translateLabel.Size = new System.Drawing.Size(80, 13);
			this.translateLabel.TabIndex = 9;
			this.translateLabel.Text = "Перемещение";
			// 
			// scaleZnNumericUpDown
			// 
			this.scaleZnNumericUpDown.DecimalPlaces = 1;
			this.scaleZnNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.scaleZnNumericUpDown.Location = new System.Drawing.Point(213, 80);
			this.scaleZnNumericUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.scaleZnNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.scaleZnNumericUpDown.Name = "scaleZnNumericUpDown";
			this.scaleZnNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.scaleZnNumericUpDown.TabIndex = 10;
			this.scaleZnNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// scaleLabel
			// 
			this.scaleLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.scaleLabel.AutoSize = true;
			this.scaleLabel.Location = new System.Drawing.Point(29, 86);
			this.scaleLabel.Name = "scaleLabel";
			this.scaleLabel.Size = new System.Drawing.Size(53, 13);
			this.scaleLabel.TabIndex = 11;
			this.scaleLabel.Text = "Масштаб";
			// 
			// angleLabel
			// 
			this.angleLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.angleLabel.AutoSize = true;
			this.angleLabel.Location = new System.Drawing.Point(267, 0);
			this.angleLabel.Name = "angleLabel";
			this.angleLabel.Size = new System.Drawing.Size(32, 13);
			this.angleLabel.TabIndex = 12;
			this.angleLabel.Text = "Угол";
			// 
			// scaleYNumericUpDown
			// 
			this.scaleYNumericUpDown.DecimalPlaces = 1;
			this.scaleYNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.scaleYNumericUpDown.Location = new System.Drawing.Point(164, 80);
			this.scaleYNumericUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.scaleYNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.scaleYNumericUpDown.Name = "scaleYNumericUpDown";
			this.scaleYNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.scaleYNumericUpDown.TabIndex = 13;
			this.scaleYNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// scaleXNumericUpDown
			// 
			this.scaleXNumericUpDown.DecimalPlaces = 1;
			this.scaleXNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.scaleXNumericUpDown.Location = new System.Drawing.Point(115, 80);
			this.scaleXNumericUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.scaleXNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.scaleXNumericUpDown.Name = "scaleXNumericUpDown";
			this.scaleXNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.scaleXNumericUpDown.TabIndex = 14;
			this.scaleXNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// centerRotateAngleNumericUpDown
			// 
			this.centerRotateAngleNumericUpDown.Location = new System.Drawing.Point(262, 176);
			this.centerRotateAngleNumericUpDown.Name = "centerRotateAngleNumericUpDown";
			this.centerRotateAngleNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.centerRotateAngleNumericUpDown.TabIndex = 15;
			// 
			// xLabel
			// 
			this.xLabel.AutoSize = true;
			this.xLabel.Location = new System.Drawing.Point(115, 0);
			this.xLabel.Name = "xLabel";
			this.xLabel.Size = new System.Drawing.Size(14, 13);
			this.xLabel.TabIndex = 16;
			this.xLabel.Text = "X";
			// 
			// yLabel
			// 
			this.yLabel.AutoSize = true;
			this.yLabel.Location = new System.Drawing.Point(164, 0);
			this.yLabel.Name = "yLabel";
			this.yLabel.Size = new System.Drawing.Size(14, 13);
			this.yLabel.TabIndex = 17;
			this.yLabel.Text = "Y";
			// 
			// zLabel
			// 
			this.zLabel.AutoSize = true;
			this.zLabel.Location = new System.Drawing.Point(213, 0);
			this.zLabel.Name = "zLabel";
			this.zLabel.Size = new System.Drawing.Size(14, 13);
			this.zLabel.TabIndex = 18;
			this.zLabel.Text = "Z";
			// 
			// rotateZNumericUpDown
			// 
			this.rotateZNumericUpDown.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
			this.rotateZNumericUpDown.Location = new System.Drawing.Point(213, 48);
			this.rotateZNumericUpDown.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
			this.rotateZNumericUpDown.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
			this.rotateZNumericUpDown.Name = "rotateZNumericUpDown";
			this.rotateZNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.rotateZNumericUpDown.TabIndex = 19;
			// 
			// rotateYNumericUpDown
			// 
			this.rotateYNumericUpDown.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
			this.rotateYNumericUpDown.Location = new System.Drawing.Point(164, 48);
			this.rotateYNumericUpDown.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
			this.rotateYNumericUpDown.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
			this.rotateYNumericUpDown.Name = "rotateYNumericUpDown";
			this.rotateYNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.rotateYNumericUpDown.TabIndex = 20;
			// 
			// rotateXNumericUpDown
			// 
			this.rotateXNumericUpDown.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
			this.rotateXNumericUpDown.Location = new System.Drawing.Point(115, 48);
			this.rotateXNumericUpDown.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
			this.rotateXNumericUpDown.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
			this.rotateXNumericUpDown.Name = "rotateXNumericUpDown";
			this.rotateXNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.rotateXNumericUpDown.TabIndex = 21;
			// 
			// rotateLabel
			// 
			this.rotateLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.rotateLabel.AutoSize = true;
			this.rotateLabel.BackColor = System.Drawing.SystemColors.Control;
			this.rotateLabel.Location = new System.Drawing.Point(31, 54);
			this.rotateLabel.Name = "rotateLabel";
			this.rotateLabel.Size = new System.Drawing.Size(50, 13);
			this.rotateLabel.TabIndex = 22;
			this.rotateLabel.Text = "Поворот";
			// 
			// translateXNumericUpDown
			// 
			this.translateXNumericUpDown.DecimalPlaces = 2;
			this.translateXNumericUpDown.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
			this.translateXNumericUpDown.Location = new System.Drawing.Point(115, 16);
			this.translateXNumericUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.translateXNumericUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
			this.translateXNumericUpDown.Name = "translateXNumericUpDown";
			this.translateXNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.translateXNumericUpDown.TabIndex = 23;
			// 
			// translateYNumericUpDown
			// 
			this.translateYNumericUpDown.DecimalPlaces = 2;
			this.translateYNumericUpDown.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
			this.translateYNumericUpDown.Location = new System.Drawing.Point(164, 16);
			this.translateYNumericUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.translateYNumericUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
			this.translateYNumericUpDown.Name = "translateYNumericUpDown";
			this.translateYNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.translateYNumericUpDown.TabIndex = 24;
			// 
			// translateZNumericUpDown
			// 
			this.translateZNumericUpDown.DecimalPlaces = 2;
			this.translateZNumericUpDown.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
			this.translateZNumericUpDown.Location = new System.Drawing.Point(213, 16);
			this.translateZNumericUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.translateZNumericUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
			this.translateZNumericUpDown.Name = "translateZNumericUpDown";
			this.translateZNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.translateZNumericUpDown.TabIndex = 25;
			// 
			// translateButton
			// 
			this.translateButton.AutoSize = true;
			this.translateButton.Location = new System.Drawing.Point(311, 16);
			this.translateButton.Name = "translateButton";
			this.translateButton.Size = new System.Drawing.Size(81, 26);
			this.translateButton.TabIndex = 26;
			this.translateButton.Text = "Применить";
			this.translateButton.UseVisualStyleBackColor = true;
			this.translateButton.Click += new System.EventHandler(this.translateButton_Click);
			// 
			// rotateButton
			// 
			this.rotateButton.AutoSize = true;
			this.rotateButton.Location = new System.Drawing.Point(311, 48);
			this.rotateButton.Name = "rotateButton";
			this.rotateButton.Size = new System.Drawing.Size(81, 26);
			this.rotateButton.TabIndex = 27;
			this.rotateButton.Text = "Применить";
			this.rotateButton.UseVisualStyleBackColor = true;
			this.rotateButton.Click += new System.EventHandler(this.rotateButton_Click);
			// 
			// scaleButton
			// 
			this.scaleButton.AutoSize = true;
			this.scaleButton.Location = new System.Drawing.Point(311, 80);
			this.scaleButton.Name = "scaleButton";
			this.scaleButton.Size = new System.Drawing.Size(81, 26);
			this.scaleButton.TabIndex = 28;
			this.scaleButton.Text = "Применить";
			this.scaleButton.UseVisualStyleBackColor = true;
			this.scaleButton.Click += new System.EventHandler(this.scaleButton_Click);
			// 
			// centerScaleLabel
			// 
			this.centerScaleLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.centerScaleLabel.AutoSize = true;
			this.centerScaleLabel.Location = new System.Drawing.Point(10, 150);
			this.centerScaleLabel.Name = "centerScaleLabel";
			this.centerScaleLabel.Size = new System.Drawing.Size(91, 13);
			this.centerScaleLabel.TabIndex = 29;
			this.centerScaleLabel.Text = "Масштаб (центр)";
			// 
			// centerScaleButton
			// 
			this.centerScaleButton.AutoSize = true;
			this.centerScaleButton.Location = new System.Drawing.Point(311, 144);
			this.centerScaleButton.Name = "centerScaleButton";
			this.centerScaleButton.Size = new System.Drawing.Size(81, 26);
			this.centerScaleButton.TabIndex = 30;
			this.centerScaleButton.Text = "Применить";
			this.centerScaleButton.UseVisualStyleBackColor = true;
			this.centerScaleButton.Click += new System.EventHandler(this.centerScaleButton_Click);
			// 
			// centerRotateLabel
			// 
			this.centerRotateLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.centerRotateLabel.AutoSize = true;
			this.centerRotateLabel.Location = new System.Drawing.Point(12, 182);
			this.centerRotateLabel.Name = "centerRotateLabel";
			this.centerRotateLabel.Size = new System.Drawing.Size(88, 13);
			this.centerRotateLabel.TabIndex = 31;
			this.centerRotateLabel.Text = "Поворот (центр)";
			// 
			// centerScaleYNumericUpDown
			// 
			this.centerScaleYNumericUpDown.DecimalPlaces = 1;
			this.centerScaleYNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.centerScaleYNumericUpDown.Location = new System.Drawing.Point(164, 144);
			this.centerScaleYNumericUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.centerScaleYNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.centerScaleYNumericUpDown.Name = "centerScaleYNumericUpDown";
			this.centerScaleYNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.centerScaleYNumericUpDown.TabIndex = 32;
			this.centerScaleYNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// centerScaleXNumericUpDown
			// 
			this.centerScaleXNumericUpDown.DecimalPlaces = 1;
			this.centerScaleXNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.centerScaleXNumericUpDown.Location = new System.Drawing.Point(115, 144);
			this.centerScaleXNumericUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.centerScaleXNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.centerScaleXNumericUpDown.Name = "centerScaleXNumericUpDown";
			this.centerScaleXNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.centerScaleXNumericUpDown.TabIndex = 33;
			this.centerScaleXNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// centerScaleZNumericUpDown
			// 
			this.centerScaleZNumericUpDown.DecimalPlaces = 1;
			this.centerScaleZNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.centerScaleZNumericUpDown.Location = new System.Drawing.Point(213, 144);
			this.centerScaleZNumericUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.centerScaleZNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.centerScaleZNumericUpDown.Name = "centerScaleZNumericUpDown";
			this.centerScaleZNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.centerScaleZNumericUpDown.TabIndex = 34;
			this.centerScaleZNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// panel1
			// 
			this.panel1.AutoSize = true;
			this.transformationsTableLayoutPanel.SetColumnSpan(this.panel1, 3);
			this.panel1.Controls.Add(this.reflectXRadioButton);
			this.panel1.Controls.Add(this.reflectYRadioButton);
			this.panel1.Controls.Add(this.reflectZRadioButton);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(115, 112);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(141, 26);
			this.panel1.TabIndex = 35;
			// 
			// reflectXRadioButton
			// 
			this.reflectXRadioButton.AutoSize = true;
			this.reflectXRadioButton.Checked = true;
			this.reflectXRadioButton.Location = new System.Drawing.Point(0, 4);
			this.reflectXRadioButton.Name = "reflectXRadioButton";
			this.reflectXRadioButton.Size = new System.Drawing.Size(32, 17);
			this.reflectXRadioButton.TabIndex = 0;
			this.reflectXRadioButton.TabStop = true;
			this.reflectXRadioButton.Text = "X";
			this.reflectXRadioButton.UseVisualStyleBackColor = true;
			// 
			// reflectYRadioButton
			// 
			this.reflectYRadioButton.AutoSize = true;
			this.reflectYRadioButton.Location = new System.Drawing.Point(48, 4);
			this.reflectYRadioButton.Name = "reflectYRadioButton";
			this.reflectYRadioButton.Size = new System.Drawing.Size(32, 17);
			this.reflectYRadioButton.TabIndex = 1;
			this.reflectYRadioButton.Text = "Y";
			this.reflectYRadioButton.UseVisualStyleBackColor = true;
			// 
			// reflectZRadioButton
			// 
			this.reflectZRadioButton.AutoSize = true;
			this.reflectZRadioButton.Location = new System.Drawing.Point(96, 4);
			this.reflectZRadioButton.Name = "reflectZRadioButton";
			this.reflectZRadioButton.Size = new System.Drawing.Size(32, 17);
			this.reflectZRadioButton.TabIndex = 2;
			this.reflectZRadioButton.Text = "Z";
			this.reflectZRadioButton.UseVisualStyleBackColor = true;
			// 
			// panel2
			// 
			this.panel2.AutoSize = true;
			this.transformationsTableLayoutPanel.SetColumnSpan(this.panel2, 3);
			this.panel2.Controls.Add(this.centerRotateXRadioButton);
			this.panel2.Controls.Add(this.centerRotateYRadioButton);
			this.panel2.Controls.Add(this.centerRotateZRadioButton);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(115, 176);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(141, 26);
			this.panel2.TabIndex = 36;
			// 
			// centerRotateXRadioButton
			// 
			this.centerRotateXRadioButton.AutoSize = true;
			this.centerRotateXRadioButton.Checked = true;
			this.centerRotateXRadioButton.Location = new System.Drawing.Point(0, 4);
			this.centerRotateXRadioButton.Name = "centerRotateXRadioButton";
			this.centerRotateXRadioButton.Size = new System.Drawing.Size(32, 17);
			this.centerRotateXRadioButton.TabIndex = 0;
			this.centerRotateXRadioButton.TabStop = true;
			this.centerRotateXRadioButton.Text = "X";
			this.centerRotateXRadioButton.UseVisualStyleBackColor = true;
			// 
			// centerRotateYRadioButton
			// 
			this.centerRotateYRadioButton.AutoSize = true;
			this.centerRotateYRadioButton.Location = new System.Drawing.Point(48, 4);
			this.centerRotateYRadioButton.Name = "centerRotateYRadioButton";
			this.centerRotateYRadioButton.Size = new System.Drawing.Size(32, 17);
			this.centerRotateYRadioButton.TabIndex = 1;
			this.centerRotateYRadioButton.Text = "Y";
			this.centerRotateYRadioButton.UseVisualStyleBackColor = true;
			// 
			// centerRotateZRadioButton
			// 
			this.centerRotateZRadioButton.AutoSize = true;
			this.centerRotateZRadioButton.Location = new System.Drawing.Point(96, 4);
			this.centerRotateZRadioButton.Name = "centerRotateZRadioButton";
			this.centerRotateZRadioButton.Size = new System.Drawing.Size(32, 17);
			this.centerRotateZRadioButton.TabIndex = 2;
			this.centerRotateZRadioButton.Text = "Z";
			this.centerRotateZRadioButton.UseVisualStyleBackColor = true;
			// 
			// lineRotateButton
			// 
			this.lineRotateButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.lineRotateButton.AutoSize = true;
			this.lineRotateButton.Location = new System.Drawing.Point(311, 218);
			this.lineRotateButton.Name = "lineRotateButton";
			this.transformationsTableLayoutPanel.SetRowSpan(this.lineRotateButton, 2);
			this.lineRotateButton.Size = new System.Drawing.Size(81, 26);
			this.lineRotateButton.TabIndex = 37;
			this.lineRotateButton.Text = "Применить";
			this.lineRotateButton.UseVisualStyleBackColor = true;
			this.lineRotateButton.Click += new System.EventHandler(this.lineRotateButton_Click);
			// 
			// lineRotateLabel
			// 
			this.lineRotateLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.lineRotateLabel.AutoSize = true;
			this.lineRotateLabel.Location = new System.Drawing.Point(3, 224);
			this.lineRotateLabel.Name = "lineRotateLabel";
			this.transformationsTableLayoutPanel.SetRowSpan(this.lineRotateLabel, 2);
			this.lineRotateLabel.Size = new System.Drawing.Size(106, 13);
			this.lineRotateLabel.TabIndex = 38;
			this.lineRotateLabel.Text = "Вращение (прямая)";
			// 
			// lineRotateAngleNumericUpDown
			// 
			this.lineRotateAngleNumericUpDown.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.lineRotateAngleNumericUpDown.Location = new System.Drawing.Point(262, 208);
			this.lineRotateAngleNumericUpDown.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
			this.lineRotateAngleNumericUpDown.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
			this.lineRotateAngleNumericUpDown.Name = "lineRotateAngleNumericUpDown";
			this.lineRotateAngleNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.lineRotateAngleNumericUpDown.TabIndex = 39;
			// 
			// cameraGroupBox
			// 
			this.cameraGroupBox.AutoSize = true;
			this.cameraGroupBox.Controls.Add(this.tableLayoutPanel1);
			this.cameraGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.cameraGroupBox.Location = new System.Drawing.Point(3, 532);
			this.cameraGroupBox.Name = "cameraGroupBox";
			this.cameraGroupBox.Size = new System.Drawing.Size(401, 218);
			this.cameraGroupBox.TabIndex = 1;
			this.cameraGroupBox.TabStop = false;
			this.cameraGroupBox.Text = "Камера";
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.AutoScroll = true;
			this.tableLayoutPanel1.AutoSize = true;
			this.tableLayoutPanel1.ColumnCount = 6;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 5);
			this.tableLayoutPanel1.Controls.Add(this.cameraXLabel, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.cameraYLabel, 2, 0);
			this.tableLayoutPanel1.Controls.Add(this.cameraZLabel, 3, 0);
			this.tableLayoutPanel1.Controls.Add(this.cameraAngleLabel, 4, 0);
			this.tableLayoutPanel1.Controls.Add(this.cameraPositionXNumericUpDown, 1, 1);
			this.tableLayoutPanel1.Controls.Add(this.cameraPositionYNumericUpDown, 2, 1);
			this.tableLayoutPanel1.Controls.Add(this.cameraPositionZNumericUpDown, 3, 1);
			this.tableLayoutPanel1.Controls.Add(this.cameraPositionLabel, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.cameraDirectionZNumericUpDown, 3, 3);
			this.tableLayoutPanel1.Controls.Add(this.cameraDirectionYNumericUpDown, 2, 3);
			this.tableLayoutPanel1.Controls.Add(this.cameraDirectionXNumericUpDown, 1, 3);
			this.tableLayoutPanel1.Controls.Add(this.cameraDirectionLabel, 0, 3);
			this.tableLayoutPanel1.Controls.Add(this.cameraPointRotateLabel, 0, 4);
			this.tableLayoutPanel1.Controls.Add(this.cameraPointRotateXNumericUpDown, 1, 4);
			this.tableLayoutPanel1.Controls.Add(this.cameraPointRotateYNumericUpDown, 2, 4);
			this.tableLayoutPanel1.Controls.Add(this.cameraPointRotateZNumericUpDown, 3, 4);
			this.tableLayoutPanel1.Controls.Add(this.fovLabel, 4, 6);
			this.tableLayoutPanel1.Controls.Add(this.fovNumericUpDown, 5, 6);
			this.tableLayoutPanel1.Controls.Add(this.cameraPointRotateButton, 5, 4);
			this.tableLayoutPanel1.Controls.Add(this.cameraPointRotateAngleNumericUpDown, 4, 4);
			this.tableLayoutPanel1.Controls.Add(this.cameraRollNumericUpDown, 5, 7);
			this.tableLayoutPanel1.Controls.Add(this.cameraRollLabel, 4, 7);
			this.tableLayoutPanel1.Controls.Add(this.cameraRotationLabel, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.cameraRotationXNumericUpDown, 1, 2);
			this.tableLayoutPanel1.Controls.Add(this.cameraRotationYNumericUpDown, 2, 2);
			this.tableLayoutPanel1.Controls.Add(this.cameraRotationZNumericUpDown, 3, 2);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 9;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 17F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(395, 199);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// panel3
			// 
			this.panel3.AutoSize = true;
			this.tableLayoutPanel1.SetColumnSpan(this.panel3, 3);
			this.panel3.Controls.Add(this.cameraPointRotateXRadioButton);
			this.panel3.Controls.Add(this.cameraPointRotateYRadioButton);
			this.panel3.Controls.Add(this.cameraPointRotateZRadioButton);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel3.Location = new System.Drawing.Point(87, 120);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(141, 24);
			this.panel3.TabIndex = 37;
			// 
			// cameraPointRotateXRadioButton
			// 
			this.cameraPointRotateXRadioButton.AutoSize = true;
			this.cameraPointRotateXRadioButton.Location = new System.Drawing.Point(0, 4);
			this.cameraPointRotateXRadioButton.Name = "cameraPointRotateXRadioButton";
			this.cameraPointRotateXRadioButton.Size = new System.Drawing.Size(32, 17);
			this.cameraPointRotateXRadioButton.TabIndex = 0;
			this.cameraPointRotateXRadioButton.Text = "X";
			this.cameraPointRotateXRadioButton.UseVisualStyleBackColor = true;
			// 
			// cameraPointRotateYRadioButton
			// 
			this.cameraPointRotateYRadioButton.AutoSize = true;
			this.cameraPointRotateYRadioButton.Checked = true;
			this.cameraPointRotateYRadioButton.Location = new System.Drawing.Point(48, 4);
			this.cameraPointRotateYRadioButton.Name = "cameraPointRotateYRadioButton";
			this.cameraPointRotateYRadioButton.Size = new System.Drawing.Size(32, 17);
			this.cameraPointRotateYRadioButton.TabIndex = 1;
			this.cameraPointRotateYRadioButton.TabStop = true;
			this.cameraPointRotateYRadioButton.Text = "Y";
			this.cameraPointRotateYRadioButton.UseVisualStyleBackColor = true;
			// 
			// cameraPointRotateZRadioButton
			// 
			this.cameraPointRotateZRadioButton.AutoSize = true;
			this.cameraPointRotateZRadioButton.Location = new System.Drawing.Point(96, 4);
			this.cameraPointRotateZRadioButton.Name = "cameraPointRotateZRadioButton";
			this.cameraPointRotateZRadioButton.Size = new System.Drawing.Size(32, 17);
			this.cameraPointRotateZRadioButton.TabIndex = 2;
			this.cameraPointRotateZRadioButton.Text = "Z";
			this.cameraPointRotateZRadioButton.UseVisualStyleBackColor = true;
			// 
			// cameraXLabel
			// 
			this.cameraXLabel.AutoSize = true;
			this.cameraXLabel.Location = new System.Drawing.Point(87, 0);
			this.cameraXLabel.Name = "cameraXLabel";
			this.cameraXLabel.Size = new System.Drawing.Size(14, 13);
			this.cameraXLabel.TabIndex = 0;
			this.cameraXLabel.Text = "X";
			// 
			// cameraYLabel
			// 
			this.cameraYLabel.AutoSize = true;
			this.cameraYLabel.Location = new System.Drawing.Point(136, 0);
			this.cameraYLabel.Name = "cameraYLabel";
			this.cameraYLabel.Size = new System.Drawing.Size(14, 13);
			this.cameraYLabel.TabIndex = 1;
			this.cameraYLabel.Text = "Y";
			// 
			// cameraZLabel
			// 
			this.cameraZLabel.AutoSize = true;
			this.cameraZLabel.Location = new System.Drawing.Point(185, 0);
			this.cameraZLabel.Name = "cameraZLabel";
			this.cameraZLabel.Size = new System.Drawing.Size(14, 13);
			this.cameraZLabel.TabIndex = 2;
			this.cameraZLabel.Text = "Z";
			// 
			// cameraAngleLabel
			// 
			this.cameraAngleLabel.AutoSize = true;
			this.cameraAngleLabel.Location = new System.Drawing.Point(234, 0);
			this.cameraAngleLabel.Name = "cameraAngleLabel";
			this.cameraAngleLabel.Size = new System.Drawing.Size(32, 13);
			this.cameraAngleLabel.TabIndex = 3;
			this.cameraAngleLabel.Text = "Угол";
			// 
			// cameraPositionXNumericUpDown
			// 
			this.cameraPositionXNumericUpDown.DecimalPlaces = 1;
			this.cameraPositionXNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.cameraPositionXNumericUpDown.Location = new System.Drawing.Point(87, 16);
			this.cameraPositionXNumericUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.cameraPositionXNumericUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
			this.cameraPositionXNumericUpDown.Name = "cameraPositionXNumericUpDown";
			this.cameraPositionXNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.cameraPositionXNumericUpDown.TabIndex = 4;
			this.cameraPositionXNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.cameraPositionXNumericUpDown.ValueChanged += new System.EventHandler(this.cameraPositionXNumericUpDown_ValueChanged);
			// 
			// cameraPositionYNumericUpDown
			// 
			this.cameraPositionYNumericUpDown.DecimalPlaces = 1;
			this.cameraPositionYNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.cameraPositionYNumericUpDown.Location = new System.Drawing.Point(136, 16);
			this.cameraPositionYNumericUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.cameraPositionYNumericUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
			this.cameraPositionYNumericUpDown.Name = "cameraPositionYNumericUpDown";
			this.cameraPositionYNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.cameraPositionYNumericUpDown.TabIndex = 6;
			this.cameraPositionYNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.cameraPositionYNumericUpDown.ValueChanged += new System.EventHandler(this.cameraPositionYNumericUpDown_ValueChanged);
			// 
			// cameraPositionZNumericUpDown
			// 
			this.cameraPositionZNumericUpDown.DecimalPlaces = 1;
			this.cameraPositionZNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.cameraPositionZNumericUpDown.Location = new System.Drawing.Point(185, 16);
			this.cameraPositionZNumericUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.cameraPositionZNumericUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
			this.cameraPositionZNumericUpDown.Name = "cameraPositionZNumericUpDown";
			this.cameraPositionZNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.cameraPositionZNumericUpDown.TabIndex = 7;
			this.cameraPositionZNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.cameraPositionZNumericUpDown.ValueChanged += new System.EventHandler(this.cameraPositionZNumericUpDown_ValueChanged);
			// 
			// cameraPositionLabel
			// 
			this.cameraPositionLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.cameraPositionLabel.AutoSize = true;
			this.cameraPositionLabel.Location = new System.Drawing.Point(16, 19);
			this.cameraPositionLabel.Name = "cameraPositionLabel";
			this.cameraPositionLabel.Size = new System.Drawing.Size(51, 13);
			this.cameraPositionLabel.TabIndex = 5;
			this.cameraPositionLabel.Text = "Позиция";
			// 
			// cameraDirectionZNumericUpDown
			// 
			this.cameraDirectionZNumericUpDown.DecimalPlaces = 1;
			this.cameraDirectionZNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.cameraDirectionZNumericUpDown.Location = new System.Drawing.Point(185, 68);
			this.cameraDirectionZNumericUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.cameraDirectionZNumericUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
			this.cameraDirectionZNumericUpDown.Name = "cameraDirectionZNumericUpDown";
			this.cameraDirectionZNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.cameraDirectionZNumericUpDown.TabIndex = 11;
			this.cameraDirectionZNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.cameraDirectionZNumericUpDown.ValueChanged += new System.EventHandler(this.cameraDirectionZNumericUpDown_ValueChanged);
			// 
			// cameraDirectionYNumericUpDown
			// 
			this.cameraDirectionYNumericUpDown.DecimalPlaces = 1;
			this.cameraDirectionYNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.cameraDirectionYNumericUpDown.Location = new System.Drawing.Point(136, 68);
			this.cameraDirectionYNumericUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.cameraDirectionYNumericUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
			this.cameraDirectionYNumericUpDown.Name = "cameraDirectionYNumericUpDown";
			this.cameraDirectionYNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.cameraDirectionYNumericUpDown.TabIndex = 10;
			this.cameraDirectionYNumericUpDown.ValueChanged += new System.EventHandler(this.cameraDirectionYNumericUpDown_ValueChanged);
			// 
			// cameraDirectionXNumericUpDown
			// 
			this.cameraDirectionXNumericUpDown.DecimalPlaces = 1;
			this.cameraDirectionXNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.cameraDirectionXNumericUpDown.Location = new System.Drawing.Point(87, 68);
			this.cameraDirectionXNumericUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.cameraDirectionXNumericUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
			this.cameraDirectionXNumericUpDown.Name = "cameraDirectionXNumericUpDown";
			this.cameraDirectionXNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.cameraDirectionXNumericUpDown.TabIndex = 9;
			this.cameraDirectionXNumericUpDown.ValueChanged += new System.EventHandler(this.cameraDirectionXNumericUpDown_ValueChanged);
			// 
			// cameraDirectionLabel
			// 
			this.cameraDirectionLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.cameraDirectionLabel.AutoSize = true;
			this.cameraDirectionLabel.Location = new System.Drawing.Point(4, 71);
			this.cameraDirectionLabel.Name = "cameraDirectionLabel";
			this.cameraDirectionLabel.Size = new System.Drawing.Size(75, 13);
			this.cameraDirectionLabel.TabIndex = 8;
			this.cameraDirectionLabel.Text = "Направление";
			// 
			// cameraPointRotateLabel
			// 
			this.cameraPointRotateLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.cameraPointRotateLabel.AutoSize = true;
			this.cameraPointRotateLabel.Location = new System.Drawing.Point(3, 106);
			this.cameraPointRotateLabel.Name = "cameraPointRotateLabel";
			this.tableLayoutPanel1.SetRowSpan(this.cameraPointRotateLabel, 2);
			this.cameraPointRotateLabel.Size = new System.Drawing.Size(78, 26);
			this.cameraPointRotateLabel.TabIndex = 15;
			this.cameraPointRotateLabel.Text = "Вращение\r\n(вокруг точки)";
			this.cameraPointRotateLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// cameraPointRotateXNumericUpDown
			// 
			this.cameraPointRotateXNumericUpDown.DecimalPlaces = 1;
			this.cameraPointRotateXNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.cameraPointRotateXNumericUpDown.Location = new System.Drawing.Point(87, 94);
			this.cameraPointRotateXNumericUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.cameraPointRotateXNumericUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
			this.cameraPointRotateXNumericUpDown.Name = "cameraPointRotateXNumericUpDown";
			this.cameraPointRotateXNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.cameraPointRotateXNumericUpDown.TabIndex = 9;
			// 
			// cameraPointRotateYNumericUpDown
			// 
			this.cameraPointRotateYNumericUpDown.DecimalPlaces = 1;
			this.cameraPointRotateYNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.cameraPointRotateYNumericUpDown.Location = new System.Drawing.Point(136, 94);
			this.cameraPointRotateYNumericUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.cameraPointRotateYNumericUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
			this.cameraPointRotateYNumericUpDown.Name = "cameraPointRotateYNumericUpDown";
			this.cameraPointRotateYNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.cameraPointRotateYNumericUpDown.TabIndex = 9;
			// 
			// cameraPointRotateZNumericUpDown
			// 
			this.cameraPointRotateZNumericUpDown.DecimalPlaces = 1;
			this.cameraPointRotateZNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.cameraPointRotateZNumericUpDown.Location = new System.Drawing.Point(185, 94);
			this.cameraPointRotateZNumericUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.cameraPointRotateZNumericUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
			this.cameraPointRotateZNumericUpDown.Name = "cameraPointRotateZNumericUpDown";
			this.cameraPointRotateZNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.cameraPointRotateZNumericUpDown.TabIndex = 9;
			// 
			// fovLabel
			// 
			this.fovLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.fovLabel.AutoSize = true;
			this.fovLabel.Location = new System.Drawing.Point(241, 153);
			this.fovLabel.Name = "fovLabel";
			this.fovLabel.Size = new System.Drawing.Size(28, 13);
			this.fovLabel.TabIndex = 13;
			this.fovLabel.Text = "FOV";
			// 
			// fovNumericUpDown
			// 
			this.fovNumericUpDown.DecimalPlaces = 1;
			this.fovNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.fovNumericUpDown.Location = new System.Drawing.Point(283, 150);
			this.fovNumericUpDown.Name = "fovNumericUpDown";
			this.fovNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.fovNumericUpDown.TabIndex = 14;
			this.fovNumericUpDown.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
			this.fovNumericUpDown.ValueChanged += new System.EventHandler(this.fovNumericUpDown_ValueChanged);
			// 
			// cameraPointRotateButton
			// 
			this.cameraPointRotateButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.cameraPointRotateButton.AutoSize = true;
			this.cameraPointRotateButton.Location = new System.Drawing.Point(283, 106);
			this.cameraPointRotateButton.Name = "cameraPointRotateButton";
			this.tableLayoutPanel1.SetRowSpan(this.cameraPointRotateButton, 2);
			this.cameraPointRotateButton.Size = new System.Drawing.Size(81, 26);
			this.cameraPointRotateButton.TabIndex = 37;
			this.cameraPointRotateButton.Text = "Применить";
			this.cameraPointRotateButton.UseVisualStyleBackColor = true;
			this.cameraPointRotateButton.Click += new System.EventHandler(this.cameraPointRotateButton_Click);
			// 
			// cameraPointRotateAngleNumericUpDown
			// 
			this.cameraPointRotateAngleNumericUpDown.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.cameraPointRotateAngleNumericUpDown.Location = new System.Drawing.Point(234, 109);
			this.cameraPointRotateAngleNumericUpDown.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
			this.cameraPointRotateAngleNumericUpDown.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
			this.cameraPointRotateAngleNumericUpDown.Name = "cameraPointRotateAngleNumericUpDown";
			this.tableLayoutPanel1.SetRowSpan(this.cameraPointRotateAngleNumericUpDown, 2);
			this.cameraPointRotateAngleNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.cameraPointRotateAngleNumericUpDown.TabIndex = 39;
			// 
			// cameraRollNumericUpDown
			// 
			this.cameraRollNumericUpDown.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
			this.cameraRollNumericUpDown.Location = new System.Drawing.Point(283, 176);
			this.cameraRollNumericUpDown.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
			this.cameraRollNumericUpDown.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
			this.cameraRollNumericUpDown.Name = "cameraRollNumericUpDown";
			this.cameraRollNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.cameraRollNumericUpDown.TabIndex = 8;
			this.cameraRollNumericUpDown.ValueChanged += new System.EventHandler(this.cameraRollNumericUpDown_ValueChanged);
			// 
			// cameraRollLabel
			// 
			this.cameraRollLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.cameraRollLabel.AutoSize = true;
			this.cameraRollLabel.Location = new System.Drawing.Point(239, 179);
			this.cameraRollLabel.Name = "cameraRollLabel";
			this.cameraRollLabel.Size = new System.Drawing.Size(32, 13);
			this.cameraRollLabel.TabIndex = 13;
			this.cameraRollLabel.Text = "Крен";
			// 
			// cameraRotationLabel
			// 
			this.cameraRotationLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.cameraRotationLabel.AutoSize = true;
			this.cameraRotationLabel.Location = new System.Drawing.Point(17, 45);
			this.cameraRotationLabel.Name = "cameraRotationLabel";
			this.cameraRotationLabel.Size = new System.Drawing.Size(50, 13);
			this.cameraRotationLabel.TabIndex = 9;
			this.cameraRotationLabel.Text = "Поворот";
			// 
			// cameraRotationXNumericUpDown
			// 
			this.cameraRotationXNumericUpDown.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
			this.cameraRotationXNumericUpDown.Location = new System.Drawing.Point(87, 42);
			this.cameraRotationXNumericUpDown.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
			this.cameraRotationXNumericUpDown.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
			this.cameraRotationXNumericUpDown.Name = "cameraRotationXNumericUpDown";
			this.cameraRotationXNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.cameraRotationXNumericUpDown.TabIndex = 8;
			this.cameraRotationXNumericUpDown.Value = new decimal(new int[] {
            35,
            0,
            0,
            -2147483648});
			this.cameraRotationXNumericUpDown.ValueChanged += new System.EventHandler(this.cameraRotationXNumericUpDown_ValueChanged);
			// 
			// cameraRotationYNumericUpDown
			// 
			this.cameraRotationYNumericUpDown.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
			this.cameraRotationYNumericUpDown.Location = new System.Drawing.Point(136, 42);
			this.cameraRotationYNumericUpDown.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
			this.cameraRotationYNumericUpDown.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
			this.cameraRotationYNumericUpDown.Name = "cameraRotationYNumericUpDown";
			this.cameraRotationYNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.cameraRotationYNumericUpDown.TabIndex = 8;
			this.cameraRotationYNumericUpDown.Value = new decimal(new int[] {
            135,
            0,
            0,
            0});
			this.cameraRotationYNumericUpDown.ValueChanged += new System.EventHandler(this.cameraRotationYNumericUpDown_ValueChanged);
			// 
			// cameraRotationZNumericUpDown
			// 
			this.cameraRotationZNumericUpDown.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
			this.cameraRotationZNumericUpDown.Location = new System.Drawing.Point(185, 42);
			this.cameraRotationZNumericUpDown.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
			this.cameraRotationZNumericUpDown.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
			this.cameraRotationZNumericUpDown.Name = "cameraRotationZNumericUpDown";
			this.cameraRotationZNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.cameraRotationZNumericUpDown.TabIndex = 8;
			this.cameraRotationZNumericUpDown.ValueChanged += new System.EventHandler(this.cameraRotationZNumericUpDown_ValueChanged);
			// 
			// renderingGroupBox
			// 
			this.renderingGroupBox.AutoSize = true;
			this.renderingGroupBox.Controls.Add(this.plotRotationYNumericUpDown);
			this.renderingGroupBox.Controls.Add(this.visibleOnlyCheckBox);
			this.renderingGroupBox.Controls.Add(this.renderModeLabel);
			this.renderingGroupBox.Controls.Add(this.renderModeComboBox);
			this.renderingGroupBox.Controls.Add(this.boundaryColorButton);
			this.renderingGroupBox.Controls.Add(this.ambientColorButton);
			this.renderingGroupBox.Controls.Add(this.boundaryColorLabel);
			this.renderingGroupBox.Controls.Add(this.ambientColorLabel);
			this.renderingGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.renderingGroupBox.Location = new System.Drawing.Point(3, 756);
			this.renderingGroupBox.Name = "renderingGroupBox";
			this.renderingGroupBox.Size = new System.Drawing.Size(401, 98);
			this.renderingGroupBox.TabIndex = 3;
			this.renderingGroupBox.TabStop = false;
			this.renderingGroupBox.Text = "Отрисовка";
			// 
			// visibleOnlyCheckBox
			// 
			this.visibleOnlyCheckBox.AutoSize = true;
			this.visibleOnlyCheckBox.Location = new System.Drawing.Point(5, 60);
			this.visibleOnlyCheckBox.Name = "visibleOnlyCheckBox";
			this.visibleOnlyCheckBox.Size = new System.Drawing.Size(176, 17);
			this.visibleOnlyCheckBox.TabIndex = 5;
			this.visibleOnlyCheckBox.Text = "Отсечение нелицевых граней";
			this.visibleOnlyCheckBox.UseVisualStyleBackColor = true;
			this.visibleOnlyCheckBox.CheckedChanged += new System.EventHandler(this.visibleOnlyCheckBox_CheckedChanged);
			// 
			// renderModeLabel
			// 
			this.renderModeLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.renderModeLabel.AutoSize = true;
			this.renderModeLabel.Location = new System.Drawing.Point(6, 27);
			this.renderModeLabel.Name = "renderModeLabel";
			this.renderModeLabel.Size = new System.Drawing.Size(26, 13);
			this.renderModeLabel.TabIndex = 1;
			this.renderModeLabel.Text = "Вид";
			// 
			// renderModeComboBox
			// 
			this.renderModeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.renderModeComboBox.FormattingEnabled = true;
			this.renderModeComboBox.Items.AddRange(new object[] {
            "Каркасный",
            "Сплошной",
            "Шэйдинг",
            "Текстурирование",
            "Плавающий горизонт"});
			this.renderModeComboBox.Location = new System.Drawing.Point(33, 23);
			this.renderModeComboBox.Name = "renderModeComboBox";
			this.renderModeComboBox.Size = new System.Drawing.Size(99, 21);
			this.renderModeComboBox.TabIndex = 2;
			this.renderModeComboBox.SelectedIndexChanged += new System.EventHandler(this.renderModeComboBox_SelectedIndexChanged);
			// 
			// boundaryColorButton
			// 
			this.boundaryColorButton.AutoSize = true;
			this.boundaryColorButton.Location = new System.Drawing.Point(357, 19);
			this.boundaryColorButton.Name = "boundaryColorButton";
			this.boundaryColorButton.Size = new System.Drawing.Size(38, 26);
			this.boundaryColorButton.TabIndex = 2;
			this.boundaryColorButton.UseVisualStyleBackColor = true;
			this.boundaryColorButton.Click += new System.EventHandler(this.boundaryColorButton_Click);
			// 
			// ambientColorButton
			// 
			this.ambientColorButton.AutoSize = true;
			this.ambientColorButton.Location = new System.Drawing.Point(219, 19);
			this.ambientColorButton.Name = "ambientColorButton";
			this.ambientColorButton.Size = new System.Drawing.Size(38, 26);
			this.ambientColorButton.TabIndex = 2;
			this.ambientColorButton.UseVisualStyleBackColor = true;
			this.ambientColorButton.Click += new System.EventHandler(this.ambientColorButton_Click);
			// 
			// boundaryColorLabel
			// 
			this.boundaryColorLabel.AutoSize = true;
			this.boundaryColorLabel.Location = new System.Drawing.Point(290, 26);
			this.boundaryColorLabel.Margin = new System.Windows.Forms.Padding(3, 0, 5, 0);
			this.boundaryColorLabel.Name = "boundaryColorLabel";
			this.boundaryColorLabel.Size = new System.Drawing.Size(65, 13);
			this.boundaryColorLabel.TabIndex = 3;
			this.boundaryColorLabel.Text = "Цвет рёбер";
			// 
			// ambientColorLabel
			// 
			this.ambientColorLabel.AutoSize = true;
			this.ambientColorLabel.Location = new System.Drawing.Point(156, 26);
			this.ambientColorLabel.Margin = new System.Windows.Forms.Padding(3, 0, 5, 0);
			this.ambientColorLabel.Name = "ambientColorLabel";
			this.ambientColorLabel.Size = new System.Drawing.Size(61, 13);
			this.ambientColorLabel.TabIndex = 3;
			this.ambientColorLabel.Text = "Цвет фона";
			// 
			// plotRotationYNumericUpDown
			// 
			this.plotRotationYNumericUpDown.Location = new System.Drawing.Point(221, 59);
			this.plotRotationYNumericUpDown.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
			this.plotRotationYNumericUpDown.Name = "plotRotationYNumericUpDown";
			this.plotRotationYNumericUpDown.Size = new System.Drawing.Size(120, 20);
			this.plotRotationYNumericUpDown.TabIndex = 6;
			this.plotRotationYNumericUpDown.ValueChanged += new System.EventHandler(this.plotRotationYNumericUpDown_ValueChanged);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1087, 875);
			this.Controls.Add(this.mainTableLayoutPanel);
			this.Controls.Add(this.mainMenuStrip);
			this.MainMenuStrip = this.mainMenuStrip;
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Editor3D";
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
			this.mainMenuStrip.ResumeLayout(false);
			this.mainMenuStrip.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown25)).EndInit();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown24)).EndInit();
			this.tableLayoutPanel5.ResumeLayout(false);
			this.tableLayoutPanel5.PerformLayout();
			this.mainTableLayoutPanel.ResumeLayout(false);
			this.mainTableLayoutPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.renderPictureBox)).EndInit();
			this.sideTableLayoutPanel.ResumeLayout(false);
			this.sideTableLayoutPanel.PerformLayout();
			this.sceneGroupBox.ResumeLayout(false);
			this.sceneGroupBox.PerformLayout();
			this.transformationsGroupBox.ResumeLayout(false);
			this.transformationsGroupBox.PerformLayout();
			this.transformationsTableLayoutPanel.ResumeLayout(false);
			this.transformationsTableLayoutPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.point2XNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.point1XNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.point1ZNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.point2ZNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.point1YNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.point2YNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.scaleZnNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.scaleYNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.scaleXNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.centerRotateAngleNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.rotateZNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.rotateYNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.rotateXNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.translateXNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.translateYNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.translateZNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.centerScaleYNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.centerScaleXNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.centerScaleZNumericUpDown)).EndInit();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.lineRotateAngleNumericUpDown)).EndInit();
			this.cameraGroupBox.ResumeLayout(false);
			this.cameraGroupBox.PerformLayout();
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.panel3.ResumeLayout(false);
			this.panel3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cameraPositionXNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cameraPositionYNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cameraPositionZNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cameraDirectionZNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cameraDirectionYNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cameraDirectionXNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cameraPointRotateXNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cameraPointRotateYNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cameraPointRotateZNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fovNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cameraPointRotateAngleNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cameraRollNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cameraRotationXNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cameraRotationYNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cameraRotationZNumericUpDown)).EndInit();
			this.renderingGroupBox.ResumeLayout(false);
			this.renderingGroupBox.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.plotRotationYNumericUpDown)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tetrahedronToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem icosahedronToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dodecahedronToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem plotToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotationFigureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сценаToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.NumericUpDown lineRotateAngleNumericUpDown;
        private System.Windows.Forms.RadioButton centerRotateZRadioButton;
        private System.Windows.Forms.RadioButton centerRotateYRadioButton;
        private System.Windows.Forms.RadioButton centerRotateXRadioButton;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton reflectZRadioButton;
        private System.Windows.Forms.RadioButton reflectYRadioButton;
        private System.Windows.Forms.RadioButton reflectXRadioButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.NumericUpDown centerScaleZNumericUpDown;
        private System.Windows.Forms.NumericUpDown centerScaleXNumericUpDown;
        private System.Windows.Forms.NumericUpDown centerScaleYNumericUpDown;
        private System.Windows.Forms.NumericUpDown centerRotateAngleNumericUpDown;
        private System.Windows.Forms.NumericUpDown scaleXNumericUpDown;
        private System.Windows.Forms.NumericUpDown scaleYNumericUpDown;
        private System.Windows.Forms.NumericUpDown scaleZnNumericUpDown;
        private System.Windows.Forms.NumericUpDown point2YNumericUpDown;
        private System.Windows.Forms.NumericUpDown point1YNumericUpDown;
        private System.Windows.Forms.NumericUpDown point2ZNumericUpDown;
        private System.Windows.Forms.NumericUpDown point1ZNumericUpDown;
        private System.Windows.Forms.NumericUpDown point1XNumericUpDown;
        private System.Windows.Forms.NumericUpDown point2XNumericUpDown;
        private System.Windows.Forms.NumericUpDown fovNumericUpDown;
        private System.Windows.Forms.ListBox primitivesListBox;
        private System.Windows.Forms.TableLayoutPanel mainTableLayoutPanel;
        private System.Windows.Forms.NumericUpDown numericUpDown25;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.NumericUpDown numericUpDown24;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown numericUpDown3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.MenuStrip mainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.PictureBox renderPictureBox;
        private System.Windows.Forms.GroupBox sceneGroupBox;
        private System.Windows.Forms.Button plotButton;
        private System.Windows.Forms.Button dodecahedronButton;
        private System.Windows.Forms.Button icosahedronButton;
        private System.Windows.Forms.Button tetrahedronButton;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.Button colorButton;
        private System.Windows.Forms.Label colorLabel;
        private System.Windows.Forms.GroupBox transformationsGroupBox;
        private System.Windows.Forms.TableLayoutPanel transformationsTableLayoutPanel;
        private System.Windows.Forms.Label translateLabel;
        private System.Windows.Forms.Label angleLabel;
        private System.Windows.Forms.Label xLabel;
        private System.Windows.Forms.Label yLabel;
        private System.Windows.Forms.Label zLabel;
        private System.Windows.Forms.NumericUpDown translateXNumericUpDown;
        private System.Windows.Forms.NumericUpDown translateYNumericUpDown;
        private System.Windows.Forms.NumericUpDown translateZNumericUpDown;
        private System.Windows.Forms.Button translateButton;
        private System.Windows.Forms.NumericUpDown rotateXNumericUpDown;
        private System.Windows.Forms.Label rotateLabel;
        private System.Windows.Forms.NumericUpDown rotateYNumericUpDown;
        private System.Windows.Forms.NumericUpDown rotateZNumericUpDown;
        private System.Windows.Forms.Label fovLabel;
        private System.Windows.Forms.Button centerRotateButton;
        private System.Windows.Forms.Button reflectButton;
        private System.Windows.Forms.Label reflectLabel;
        private System.Windows.Forms.Label scaleLabel;
        private System.Windows.Forms.Button rotateButton;
        private System.Windows.Forms.Button scaleButton;
        private System.Windows.Forms.Label centerScaleLabel;
        private System.Windows.Forms.Button centerScaleButton;
        private System.Windows.Forms.Label centerRotateLabel;
        private System.Windows.Forms.Label lineRotateLabel;
        private System.Windows.Forms.GroupBox cameraGroupBox;
        private System.Windows.Forms.Button lineRotateButton;
        private System.Windows.Forms.TableLayoutPanel sideTableLayoutPanel;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.Label cameraPositionLabel;
        private System.Windows.Forms.NumericUpDown cameraPositionYNumericUpDown;
        private System.Windows.Forms.NumericUpDown cameraPositionZNumericUpDown;
        private System.Windows.Forms.Label cameraRotationLabel;
        private System.Windows.Forms.NumericUpDown cameraPositionXNumericUpDown;
        private System.Windows.Forms.ComboBox renderModeComboBox;
        private System.Windows.Forms.Label renderModeLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label cameraZLabel;
        private System.Windows.Forms.Label cameraYLabel;
        private System.Windows.Forms.Label cameraXLabel;
        private System.Windows.Forms.Label cameraAngleLabel;
        private System.Windows.Forms.Label cameraDirectionLabel;
        private System.Windows.Forms.NumericUpDown cameraDirectionYNumericUpDown;
        private System.Windows.Forms.NumericUpDown cameraDirectionZNumericUpDown;
        private System.Windows.Forms.NumericUpDown cameraDirectionXNumericUpDown;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button cameraPointRotateButton;
        private System.Windows.Forms.RadioButton cameraPointRotateZRadioButton;
        private System.Windows.Forms.RadioButton cameraPointRotateYRadioButton;
        private System.Windows.Forms.RadioButton cameraPointRotateXRadioButton;
        private System.Windows.Forms.NumericUpDown cameraPointRotateZNumericUpDown;
        private System.Windows.Forms.NumericUpDown cameraPointRotateYNumericUpDown;
        private System.Windows.Forms.NumericUpDown cameraPointRotateXNumericUpDown;
        private System.Windows.Forms.NumericUpDown cameraPointRotateAngleNumericUpDown;
        private System.Windows.Forms.NumericUpDown cameraRollNumericUpDown;
        private System.Windows.Forms.Label cameraRollLabel;
        private System.Windows.Forms.NumericUpDown cameraRotationZNumericUpDown;
        private System.Windows.Forms.NumericUpDown cameraRotationYNumericUpDown;
        private System.Windows.Forms.NumericUpDown cameraRotationXNumericUpDown;
        private System.Windows.Forms.Label cameraPointRotateLabel;
        private System.Windows.Forms.GroupBox renderingGroupBox;
        private System.Windows.Forms.Label ambientColorLabel;
        private System.Windows.Forms.Button ambientColorButton;
        private System.Windows.Forms.Label boundaryColorLabel;
        private System.Windows.Forms.Button boundaryColorButton;
        private System.Windows.Forms.Button rotationFigureButton;
        private System.Windows.Forms.Button lightButton;
        private System.Windows.Forms.CheckBox visibleOnlyCheckBox;
		private NumericUpDown plotRotationYNumericUpDown;
	}
}