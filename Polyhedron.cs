﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using org.mariuszgromada.math.mxparser;

namespace Editor3D
{
    public partial class Polyhedron : IPrimitive
    {
        public List<Point3D> points = new List<Point3D>();

        public List<Facet> facets = new List<Facet>();

        public Point3D Center
        {
            get
            {
                if (points.Count == 0)
                    return null;
                double averageX = points.Average(point => point.X);
                double averageY = points.Average(point => point.Y);
                double averageZ = points.Average(point => point.Z);
                return new Point3D(averageX, averageY, averageZ);
            }
        }

        public string name;

        public Color Color
        {
            get
            {
                Color currentColor = facets[0].Color;
                foreach (Point3D point in points)
                    if (point.Color != currentColor)
                        return Color.Empty;
                return currentColor;
            }
            set
            {
                foreach (Point3D point in points)
                    point.Color = value;
            }
        }

        public Polyhedron(string name = "Polyhedron")
        {
            this.name = name;
        }

        public Polyhedron(List<Point3D> points, List<Facet> facets, string name = "Polyhedron")
        {
            this.points = points;
            this.facets = facets;
            this.name = name;
        }

        public void Apply(Transformation t)
        {
            foreach (var point in points)
                point.Apply(t);
        }

        public Polyhedron Copy()
        {
            Polyhedron copiedPolyhedron = new Polyhedron(new List<Point3D>(),
                facets.Select(facet => new Facet(facet.points.ToList())).ToList(), name);
            foreach (Point3D point in points)
            {
                Point3D copiedPoint = new Point3D(point.X, point.Y, point.Z, point.Color);
                foreach (Facet facet in copiedPolyhedron.facets)
                    if (facet.points.Contains(point))
                        facet.points[facet.points.IndexOf(point)] = copiedPoint;
                copiedPolyhedron.points.Add(copiedPoint);
            }
            return copiedPolyhedron;
        }

        public static Polyhedron Tetrahedron(double size)
        {
            return Tetrahedron(size, Color.Black);
        }

        public static Polyhedron Tetrahedron(double size, Color color)
        {
            Polyhedron polyhedron = new Polyhedron("Tetrahedron");

            double h = Math.Sqrt(2.0 / 3.0) * size;
            polyhedron.points = new List<Point3D>();

            polyhedron.points.Add(new Point3D(-size / 2, 0, h / 3, color));
            polyhedron.points.Add(new Point3D(0, 0, -h * 2 / 3, color));
            polyhedron.points.Add(new Point3D(size / 2, 0, h / 3, color));
            polyhedron.points.Add(new Point3D(0, h, 0, color));

            // Основание тетраэдра
            polyhedron.facets.Add(new Facet(new Point3D[]
            {
                polyhedron.points[0], polyhedron.points[1], polyhedron.points[2]
            }, color));
            // Левая грань
            polyhedron.facets.Add(new Facet(new Point3D[]
            {
                polyhedron.points[1], polyhedron.points[3], polyhedron.points[0]
            }, color));
            // Правая грань
            polyhedron.facets.Add(new Facet(new Point3D[]
            {
                polyhedron.points[2], polyhedron.points[3], polyhedron.points[1]
            }, color));
            // Передняя грань
            polyhedron.facets.Add(new Facet(new Point3D[]
            {
                polyhedron.points[0], polyhedron.points[3], polyhedron.points[2]
            }, color));

            return polyhedron;
        }

        public static Polyhedron Icosahedron(double size)
        {
            return Icosahedron(size, Color.Black);
        }

        public static Polyhedron Icosahedron(double size, Color color)
        {
            Polyhedron polyhedron = new Polyhedron("Icosahedron");

            // радиус описанной сферы
            double R = (size * Math.Sqrt(2.0 * (5.0 + Math.Sqrt(5.0)))) / 4;

            // радиус вписанной сферы
            double r = (size * Math.Sqrt(3.0) * (3.0 + Math.Sqrt(5.0))) / 12;

            polyhedron.points = new List<Point3D>();

            for (int i = 0; i < 5; ++i)
            {
                polyhedron.points.Add(new Point3D(
                    r * Math.Cos(2 * Math.PI / 5 * i),
                    R / 2,
                    r * Math.Sin(2 * Math.PI / 5 * i), color));
                polyhedron.points.Add(new Point3D(
                    r * Math.Cos(2 * Math.PI / 5 * i + 2 * Math.PI / 10),
                    -R / 2,
                    r * Math.Sin(2 * Math.PI / 5 * i + 2 * Math.PI / 10), color));
            }

            polyhedron.points.Add(new Point3D(0, R, 0, color));
            polyhedron.points.Add(new Point3D(0, -R, 0, color));

            // середина
            for (int i = 0; i < 10; ++i)
                polyhedron.facets.Add(new Facet(
                    new Point3D[]
                        {polyhedron.points[i], polyhedron.points[(i + 1) % 10], polyhedron.points[(i + 2) % 10]},
                    color));

            for (int i = 0; i < 5; ++i)
            {
                // верхняя часть
                polyhedron.facets.Add(new Facet(
                    new Point3D[]
                        {polyhedron.points[2 * i], polyhedron.points[10], polyhedron.points[(2 * (i + 1)) % 10]},
                    color));
                // нижняя часть
                polyhedron.facets.Add(new Facet(
                    new Point3D[]
                    {
                        polyhedron.points[2 * i + 1], polyhedron.points[11], polyhedron.points[(2 * (i + 1) + 1) % 10]
                    },
                    color));
            }

            return polyhedron;
        }

        public static Polyhedron Dodecahedron(double size)
        {
            return Dodecahedron(size, Color.Black);
        }

        public static Polyhedron Dodecahedron(double size, Color color)
        {
            void InitFacets(Polyhedron p)
            {
                // 12 граней
                p.facets.Add(new Facet(new Point3D[]
                {
                    p.points[10], p.points[12],
                    p.points[14], p.points[16], p.points[18]
                }, color));
                p.facets.Add(new Facet(new Point3D[]
                {
                    p.points[11], p.points[13],
                    p.points[15], p.points[17], p.points[19]
                }, color));
                p.facets.Add(new Facet(new Point3D[]
                {
                    p.points[0], p.points[1],
                    p.points[2], p.points[12], p.points[10]
                }, color));
                p.facets.Add(new Facet(new Point3D[]
                {
                    p.points[2], p.points[3],
                    p.points[4], p.points[14], p.points[12]
                }, color));
                p.facets.Add(new Facet(new Point3D[]
                {
                    p.points[4], p.points[5],
                    p.points[6], p.points[16], p.points[14]
                }, color));
                p.facets.Add(new Facet(new Point3D[]
                {
                    p.points[6], p.points[7],
                    p.points[8], p.points[18], p.points[16]
                }, color));
                p.facets.Add(new Facet(new Point3D[]
                {
                    p.points[8], p.points[9],
                    p.points[0], p.points[10], p.points[18]
                }, color));
                p.facets.Add(new Facet(new Point3D[]
                {
                    p.points[1], p.points[2],
                    p.points[3], p.points[13], p.points[11]
                }, color));
                p.facets.Add(new Facet(new Point3D[]
                {
                    p.points[3], p.points[4],
                    p.points[5], p.points[15], p.points[13]
                }, color));
                p.facets.Add(new Facet(new Point3D[]
                {
                    p.points[5], p.points[6],
                    p.points[7], p.points[17], p.points[15]
                }, color));
                p.facets.Add(new Facet(new Point3D[]
                {
                    p.points[7], p.points[8],
                    p.points[9], p.points[19], p.points[17]
                }, color));
                p.facets.Add(new Facet(new Point3D[]
                {
                    p.points[9], p.points[0],
                    p.points[1], p.points[11], p.points[19]
                }, color));
            }

            Polyhedron icosahedron = Polyhedron.Icosahedron(size);
            Polyhedron polyhedron = new Polyhedron("Dodecahedron");

            for (int i = 0; i < 20; i++)
            {
                double x = (icosahedron.facets[i].points[0].X
                            + icosahedron.facets[i].points[1].X
                            + icosahedron.facets[i].points[2].X) / 3;
                double y = (icosahedron.facets[i].points[0].Y
                            + icosahedron.facets[i].points[1].Y
                            + icosahedron.facets[i].points[2].Y) / 3;
                double z = (icosahedron.facets[i].points[0].Z
                            + icosahedron.facets[i].points[1].Z
                            + icosahedron.facets[i].points[2].Z) / 3;
                polyhedron.points.Add(new Point3D(x, y, z, color));
            }
            InitFacets(polyhedron);

            return polyhedron;
        }

        public static Polyhedron RotationFigure(List<Point3D> generatrix, Transformation matrix, int splits)
        {
            return Polyhedron.RotationFigure(generatrix, matrix, splits, Color.Black);
        }

        public static Polyhedron RotationFigure(List<Point3D> generatrix, Transformation matrix, int splits,
            Color color)
        {
            Polyhedron polyhedron = new Polyhedron("Rotation Figure");

            double angle = (360d / splits) * Math.PI / 180;
            List<Point3D> currentList = new List<Point3D>();
            Facet topFacet = new Facet(color);
            Facet downFacet = new Facet(color);

            for (int i = 0; i < generatrix.Count; i++)
            {
                currentList.Add(new Point3D(generatrix[i]));
                polyhedron.points.Add(new Point3D(generatrix[i]));
            }
            topFacet.points.Add(polyhedron.points[0]);
            downFacet.points.Add(polyhedron.points[polyhedron.points.Count - 1]);

            // Добавление всех граней кроме последнего слоя
            for (int i = 0; i < splits - 1; i++)
            {
                currentList[0].Apply(matrix);
                polyhedron.points.Add(new Point3D(currentList[0]));
                topFacet.points.Add(polyhedron.points[polyhedron.points.Count - 1]);
                for (int j = 1; j < currentList.Count; j++)
                {
                    currentList[j].Apply(matrix);
                    polyhedron.points.Add(new Point3D(currentList[j]));
                    Facet facet = new Facet(
                        new Point3D[]
                        {
                            polyhedron.points[polyhedron.points.Count - generatrix.Count - 2],
                            polyhedron.points[polyhedron.points.Count - generatrix.Count - 1],
                            polyhedron.points[polyhedron.points.Count - 1],
                            polyhedron.points[polyhedron.points.Count - 2]
                        }, color);
                    polyhedron.facets.Add(facet);
                }
                downFacet.points.Add(polyhedron.points[polyhedron.points.Count - 1]);
            }

            // Добавление последного слоя граней
            for (int j = 1; j < generatrix.Count; j++)
            {
                Facet facet = new Facet(
                    new Point3D[]
                    {
                        polyhedron.points[polyhedron.points.Count - generatrix.Count + j - 1],
                        polyhedron.points[polyhedron.points.Count - generatrix.Count + j],
                        polyhedron.points[j],
                        polyhedron.points[j - 1]
                    }, color);
                polyhedron.facets.Add(facet);
            }
            polyhedron.facets.Add(topFacet);
            polyhedron.facets.Add(downFacet);

            return polyhedron;
        }

        public static Polyhedron Plot(string function, double minX, double maxX, double minZ, double maxZ,
            int xTicks, int zTicks)
        {
            return Polyhedron.Plot(function, minX, maxX, minZ, maxZ, xTicks, zTicks, Color.Black);
        }
        
        public static Polyhedron Plot(string function, double minX, double maxX, double minZ, double maxZ,
            int xTicks, int zTicks, Color color)
        {
            Polyhedron polyhedron = new Polyhedron("Plot");

            Argument xArgument = new Argument("x");
            Argument zArgument = new Argument("z");
            Expression expression = new Expression(function, xArgument, zArgument);

            double deltaX = (maxX - minX) / xTicks;
            double deltaZ = (maxZ - minZ) / zTicks;

            // Добавляем точки.
            double z = minZ;
            for (int i = 0; i <= zTicks; i++)
            {
                double x = minX;
                for (int j = 0; j <= xTicks; j++)
                {
                    xArgument.setArgumentValue(x);
                    zArgument.setArgumentValue(z);
                    double y = expression.calculate();
                    polyhedron.points.Add(new Point3D(x, y, z, color));

                    x += deltaX;
                }
                z += deltaZ;
            }

            // Добавляем грани.
            for (int i = 0; i < zTicks; i++)
                for (int j = 0; j < xTicks; j++)
                    polyhedron.facets.Add(new Facet(new Point3D[]
                    {
                        polyhedron.points[i * (zTicks + 1) + j],
                        polyhedron.points[i * (zTicks + 1) + (j + 1)],
                        polyhedron.points[(i + 1) * (zTicks + 1) + (j + 1)],
                        polyhedron.points[(i + 1) * (zTicks + 1) + j]
                    }, color));

            return polyhedron;
        }
    }
}