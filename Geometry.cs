﻿using System;
using System.Windows.Media.Media3D;

namespace Editor3D
{
    public partial class Geometry
    {
        public static double GetAngle(double sin, double cos)
        {
            if (cos >= 0)
                return Math.Asin(sin);
            else if (sin >= 0)
                return Math.PI - Math.Asin(sin);
            else
                return -Math.PI - Math.Asin(sin);
        }

        public static Vector3D NormalVector(Facet facet, Polyhedron polyhedron)
        {
            Vector3D normal;
            bool isConvex = !polyhedron.name.Equals("Rotation Figure");
            if (isConvex)
                normal = Geometry.NormalVector(facet, polyhedron.Center);
            else
            {
                normal = Geometry.NormalVector(facet);
                if (facet == polyhedron.facets[polyhedron.facets.Count - 1])
                    normal = -1 * normal;
            }
            return normal;
        }

        // возвращает неопределенную нормаль
        public static Vector3D NormalVector(Facet facet)
        {
            Point3D P0 = facet.points[0];
            Point3D P1 = facet.points[1];
            Point3D P2 = facet.points[2];
            Vector3D P1P0 = new Vector3D(P0.X - P1.X, P0.Y - P1.Y, P0.Z - P1.Z);
            Vector3D P2P0 = new Vector3D(P0.X - P2.X, P0.Y - P2.Y, P0.Z - P2.Z);

            return -Vector3D.CrossProduct(P1P0, P2P0);
        }

        // возвращает истинную нормаль, для выпуклых фигур
        public static Vector3D NormalVector(Facet facet, Point3D center)
        {
            Point3D P0 = facet.points[0];
            Point3D P1 = facet.points[1];
            Point3D P2 = facet.points[2];
            Vector3D P1P0 = new Vector3D(P0.X - P1.X, P0.Y - P1.Y, P0.Z - P1.Z);
            Vector3D P2P0 = new Vector3D(P0.X - P2.X, P0.Y - P2.Y, P0.Z - P2.Z);

            Vector3D result = Vector3D.CrossProduct(P1P0, P2P0);

            Vector3D P0CENTER = new Vector3D(center.X - P0.X, center.Y - P0.Y, center.Z - P0.Z);
            if (Vector3D.AngleBetween(result, P0CENTER) < 90)
            {
                result *= -1;
            }
            return result;
        }

        public static float LinearInterpolation(float a, float b, float t)
        {
            return (t * (b - a) + a);
        }
    }
}