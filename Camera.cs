﻿using System;
using System.Diagnostics;

namespace Editor3D
{
    public class Camera
    {
        public Point3D position = new Point3D(0, 0, 0);
        public Point3D rotation = new Point3D(0, 0, 0);
        public double fieldOfView = 0;

        public Point3D Direction
        {
            get
            {
                return new Point3D(0, 0, 1).Transform(Transformation.RotateX(rotation.X) 
                    * Transformation.RotateY(rotation.Y));
            }
            set
            {
                double directionLength = Math.Sqrt(value.X * value.X
                                                   + value.Y * value.Y
                                                   + value.Z * value.Z);
                double directionXOZProjectionLength = Math.Sqrt(value.X * value.X
                                                                + value.Z * value.Z);
                double yRotationSin, yRotationCos, xRotationSin, xRotationCos;
                if (Math.Abs(directionXOZProjectionLength) > double.Epsilon)
                {
                    yRotationSin = value.X / directionXOZProjectionLength;
                    yRotationCos = value.Z / directionXOZProjectionLength;
                }
                else
                {
                    yRotationSin = 0;
                    yRotationCos = 1;
                }
                rotation.Y = -Geometry.GetAngle(yRotationSin, yRotationCos);
                if (Math.Abs(directionLength) > double.Epsilon)
                {
                    xRotationSin = value.Y / directionLength;
                    xRotationCos = directionXOZProjectionLength / directionLength;
                }
                else
                {
                    xRotationSin = 0;
                    xRotationCos = 1;
                }
                rotation.X = Geometry.GetAngle(xRotationSin, xRotationCos);
            }
        }

        public double Roll
        {
            get => rotation.Z;
            set => rotation.Z = value;
        }

        public Transformation Projection
        {
            get
            {
                return Transformation.Translate(-position.X, -position.Y, -position.Z)
                       * Transformation.RotateY(-rotation.Y)
                       * Transformation.RotateX(-rotation.X)
                       * Transformation.RotateZ(-rotation.Z)
                       * new Transformation(new double[,]
                       {
                           {1, 0, 0, 0},
                           {0, 1, 0, 0},
                           {0, 0, 1, fieldOfView},
                           {0, 0, 0, 1}
                       });
            }
        }

        public Camera() { }

        public Camera(Point3D position, Point3D rotation, double fieldOfView)
        {
            this.position = position;
            this.rotation = rotation;
            this.fieldOfView = fieldOfView;
        }
    }
}